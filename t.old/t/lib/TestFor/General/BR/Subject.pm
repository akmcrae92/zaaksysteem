package TestFor::General::BR::Subject;

use base qw(ZSTest);
use TestSetup;

use Zaaksysteem::BR::Subject;
use Zaaksysteem::API::v1::Serializer;
use Zaaksysteem::Object::Types::Authentication;

use Authen::Passphrase;

=head1 NAME

TestFor::General::BR::Subject - Businessrules for Subject

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ./zs_prove -v t/lib/TestFor/General/BR/Subject.pm


=head1 DESCRIPTION

This is the main object to interfere with the subject subsystem of Zaaksysteem. Newstyle.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with subjects


=head2 Search

Find and search a single or multiple subjects.

=cut

sub br_subject_usage_search : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        $zs->create_bedrijf_ok(handelsnaam => 'Mintlab B.V.');

        ### USAGE
        my $bridge     = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        ### Retrieve plain subject
        my $subjects    = $bridge->search(
            {
                subject_type        => 'company',
                'subject.company'   => 'Mintlab B.V.',
                'subject.address_residence.street' => 'Donker Curtiusstraat'
            },
        );

        ok($subjects->count, 'Got at least one subject: ' . $subjects->count);

        ### Retrieve as ObjectType
        while (my $object = $subjects->next) {
            $self->_verify_company_object($object);
            is($object->subject->company, 'Mintlab B.V.', 'Got valid company name: ' . $object->subject->company);
        }
    }, 'bridge->search: got a valid company');

    $zs->zs_transaction_ok(sub {
        $zs->create_natuurlijk_persoon_ok(burgerservicenummer => '12345678', geslachtsnaam => 'Bakker');

        ### USAGE
        my $bridge     = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        ### Retrieve plain subject
        my $subjects    = $bridge->search(
            {
                subject_type    => 'person',
                'subject.family_name'     => 'Bakker',
                'subject.address_residence.street' => 'Muiderstraat'
            },
        );

        ok($subjects->count, 'Got at least one subject');

        ### Retrieve as ObjectType
        while (my $object = $subjects->next) {
            $self->_verify_person_object($object);
        }

    }, 'bridge->search: got a valid person');

    $zs->zs_transaction_ok(sub {
        $zs->create_subject_ok(
            username    => 'fritsie',
            properties  => {
                cn => 'fritsie',
                sn => 'de Boer',
                displayname => 'F. de Boer',
                givenname   => 'Fritsie',
                initials    => 'F',
                mail        => 'fritsie.devnull@zaaksysteem.nl',
                telephonenumber => '1234567890',
            }
        );

        ### USAGE
        my $bridge     = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        ### Retrieve plain subject
        my $subjects    = $bridge->search(
            {
                subject_type        => 'employee',
                'subject.surname'   => 'de Boer',
            },
        );

        ok($subjects->count, 'Got at least one subject');

        ### Retrieve as ObjectType
        while (my $object = $subjects->next) {
            $self->_verify_employee_object($object);
        }

    }, 'bridge->search: got a valid employee');

    $zs->zs_transaction_ok(sub {
        my %params = (
            'subject.company'               => 'Mintlab B.V.',
            'subject.company_type'          => '41',
            'subject.coc_number'            => '12345678',
            'subject.coc_location_number'   => '123456789012'
        );

        $zs->create_bedrijf_ok(
            handelsnaam         => 'Mintlab B.V.',
            rechtsvorm          => '41',
            dossiernummer       => '12345678',
            vestigingsnummer    => '123456789012'
        );

        ### USAGE
        my $bridge     = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        ### Check for every key if a result is found
        for my $key (keys %params) {
            my $subjects    = $bridge->search(
                {
                    subject_type => 'company',
                    $key    => $params{$key}
                },
            );

            ok($subjects->count, 'Got at least one subject with search: ' . $key);

            my $subject     = $subjects->first;

            my $okey        = $key;
            $okey =~ s/^subject\.//;

            if ($key eq 'subject.company_type') {
                is($subject->subject->$okey->code, $params{$key}, "Got valid data for $key: " . $subject->subject->$okey->label);
                next;
            }

            is($subject->subject->$okey, $params{$key}, "Got valid data for $key: " . $subject->subject->$okey);
        }

        ### Check combination of keys
        my $subjects    = $bridge->search(
            {
                subject_type        => 'company',
                'subject.company'   => 'Mintlab B.VV.',
            },
        );

        ok(!$subjects->count, 'Found no company with invalid data');

        ### Check combination of keys
        $subjects       = $bridge->search({ subject_type => 'company', %params });

        ok($subjects->count, 'Found a company with combined data');

        ### Check partial search
        ok($bridge->search(
            {
                subject_type        => 'company',
                'subject.company'   => 'Mintla'
            },
        )->count, 'Got result for partial search');

    }, 'bridge->search: company: intensive search');

    $zs->zs_transaction_ok(sub {
        my %params = (
            'subject.personal_number'      => '123456789',
            'subject.personal_number_a'    => '1987654321',
            'subject.initials'             => 'D.',
            'subject.first_names'          => 'Don',
            'subject.family_name'          => 'Fuego',
            'subject.prefix'               => 'The',
            'subject.gender'               => 'M',
            'subject.date_of_birth'        => '1982-06-05',
            'subject.use_of_name'          => 'E',
        );

        $zs->create_natuurlijk_persoon_ok(
            burgerservicenummer     => '123456789',
            a_nummer                => '1987654321',
            voorletters             => 'D.',
            voornamen               => 'Don',
            geslachtsnaam           => 'Fuego',
            voorvoegsel             => 'The',
            geslachtsaanduiding     => 'M',
            geboorteplaats          => 'Amsterdam',
            geboortedatum           => DateTime->new(year => 1982, month => 6, day => 5),
            aanhef_aanschrijving    => 'Dhr.',
            aanduiding_naamgebruik  => 'E',
        );

        ### USAGE
        my $bridge     = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        ### Check for every key if a result is found
        for my $key (keys %params) {
            my $subjects    = $bridge->search(
                {
                    subject_type        => 'person',
                    $key    => $params{$key}
                },
            );

            ok($subjects->count, 'Got at least one subject with search: ' . $key);

            next unless $subjects->count;

            my $okey = $key;
            $okey =~ s/^subject\.//;

            my $subject     = $subjects->first;

            my $ovalue      = $subject->subject->$okey;

            if (ref $ovalue) {
                $ovalue     = $ovalue->ymd;
            }
            is($ovalue, $params{$key}, "Got valid data for $key: " . $ovalue);
        }

        ### Check combination of keys
        my $subjects    = $bridge->search(
            {
                subject_type            => 'person',
                'subject.family_name'   => 'Fuego',
                'subject.first_names'   => 'Frits',
            },
        );

        ok(!$subjects->count, 'Found no person with invalid data');

        ### Check combination of keys
        $subjects       = $bridge->search({ subject_type => 'person', %params });

        ok($subjects->count, 'Found a person with combined data');

        ### Check partial search
        ok($bridge->search(
            {
                'subject_type'  => 'person',
                'subject.family_name'   => 'Fueg'
            },
        )->count, 'Got result for partial search');

    }, 'bridge->search: person: intensive search');
}

=head2 br_subject_usage_find

Creation of subjects

=cut

sub br_subject_usage_find : Tests {
    $zs->zs_transaction_ok(sub {
        my %params = (
            'subject.personal_number'      => '123456789',
            'subject.personal_number_a'    => '1987654321',
            'subject.initials'             => 'D.',
            'subject.first_names'          => 'Don',
            'subject.family_name'          => 'Fuego',
            'subject.prefix'               => 'The',
            'subject.gender'               => 'M',
            'subject.date_of_birth'        => '1982-06-05',
            'subject.use_of_name'          => 'E',
        );

        ### Make some bogus persons
        $zs->create_natuurlijk_persoon_ok(
            burgerservicenummer     => '902456789',
            a_nummer                => '9987654321',
            voorletters             => 'A.',
            voornamen               => 'Von',
            geslachtsnaam           => 'Quego',
            voorvoegsel             => 'She',
            geslachtsaanduiding     => 'M',
            geboorteplaats          => 'Alderdam',
            geboortedatum           => DateTime->new(year => 1984, month => 5, day => 3),
            aanhef_aanschrijving    => 'Mevr.',
            aanduiding_naamgebruik  => 'E',
        ) for (1..3);

        my ($np) = $zs->create_natuurlijk_persoon_ok(
            burgerservicenummer     => '123456789',
            a_nummer                => '1987654321',
            voorletters             => 'D.',
            voornamen               => 'Don',
            geslachtsnaam           => 'Fuego',
            voorvoegsel             => 'The',
            geslachtsaanduiding     => 'M',
            geboorteplaats          => 'Amsterdam',
            geboortedatum           => DateTime->new(year => 1982, month => 6, day => 5),
            aanhef_aanschrijving    => 'Dhr.',
            aanduiding_naamgebruik  => 'E',
        );

        $np->discard_changes();

        my $bridge     = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        my $subject     = $bridge->find($np->uuid);

        ### Check for every key if a result is found
        for my $key (keys %params) {
            my $okey = $key;
            $okey =~ s/^subject\.//;
            my $ovalue      = $subject->subject->$okey;

            if (ref $ovalue) {
                $ovalue     = $ovalue->ymd;
            }
            is($ovalue, $params{$key}, "Got valid data for $key: " . $ovalue);
        }
    }, 'br_subject_usage_find: Found person');
}

=head2 br_subject_usage_create

Creation of subjects

=cut

sub br_subject_usage_create : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my %params = (
            'subject_type'              => 'company',
            'subject'                   => {
                'company'       => 'Mintlab B.V',
                'coc_number'            => '12345678',
                'coc_location_number'   => '545698785412',
                'company_type'          => {
                    code    => 1,
                    label   => 'Eenmanszaak',
                },
                'address_residence'          => {
                    street          => 'Donker Curtiusstraat',
                    street_number          => 7,
                    street_number_letter   => 'a',
                    street_number_suffix   => '521',
                    city            => 'Amsterdam',
                    zipcode         => '1051JL',
                    country         => {
                        label           => 'Nederland',
                        dutch_code      => 6030,
                    }
                }
            },
            authentication  => {
                password        => 'xyzuvw',
            }
        );

        my $bridge      = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        ### When just having some params, retrieve the Subject object from the given params
        my $object      = $bridge->object_from_params(\%params);

        $self->_verify_company_object($object);

        $bridge->save($object);

        ### Retrieve and verify
        $object         = $bridge->find($object->id);

        # diag(explain($object));

        $self->_verify_against_params($object, \%params);

        # Check if object is in database
        my $company = $schema->resultset('Bedrijf')->find($object->subject->_table_id);

        is($object->subject->coc_number, $company->dossiernummer, 'Got valid kvknumber');
        ok(!$company->authenticated, 'Not an authenticated company');

        # Make it authenticated
        my $interface   = $zs->create_interface_ok(
            name => 'overheidio',
            module => 'overheidio',
            interface_config => {
                overheid_io_key => ($ENV{OVERHEID_IO_KEY} || 'test'),
                simulator       => 1,
            }
        );

        $object->set_authenticated($schema, $interface, 'ABC');

        $company = $schema->resultset('Bedrijf')->find($object->subject->_table_id);
        ok($company->authenticated, 'Got a authenticated company');
        is($company->authenticatedby, 'overheidio', 'Company authenticatedby "openkvk"');
        is($company->subscription_id->external_id, 'ABC', 'Got external subscription');


        # ### Save this object
        # $bridge->save($object);
    }, 'Tested br_subject_usage_create: object_from_params for company');

    $zs->zs_transaction_ok(sub {
        my %params = (
            subject_type    => 'person',
            subject         => {
                'personal_number'           => '123456789',
                'personal_number_a'         => '1987654321',
                'initials'                  => 'D.',
                'first_names'               => 'Don',
                'family_name'               => 'Fuego',
                'prefix'                    => 'The',
                'gender'                    => 'M',
                'date_of_birth'             => '1982-06-05',
                'use_of_name'               => 'N',
                'address_residence'         => {
                    street          => 'Muiderstraat',
                    street_number          => 42,
                    street_number_letter   => 'a',
                    street_number_suffix   => '521',
                    city            => 'Amsterdam',
                    zipcode         => '1011PZ',
                    country         => {
                        label           => 'Nederland',
                        dutch_code      => 6030,
                    }
                },
                'address_correspondence'         => {
                    street          => 'Donkerstraat',
                    street_number          => 1,
                    street_number_letter   => 'b',
                    street_number_suffix   => '522',
                    city            => 'Donkerdam',
                    zipcode         => '1011PA',
                    country         => {
                        label           => 'Nederland',
                        dutch_code      => 6030,
                    }
                },
                'partner'                       => {
                    personal_number     => '987654321',
                    personal_number_a   => '1987654323',
                    prefix              => 'de',
                    family_name         => 'Boer',
                }
            },
        );

        my $bridge      = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        ### When just having some params, retrieve the Subject object from the given params
        my $object      = $bridge->object_from_params(\%params);

        $self->_verify_person_object($object);

        $bridge->save($object);

        ### Retrieve and verify
        $object         = $bridge->find($object->id);

        is($object->subject->surname, 'The Fuego-de Boer', 'Test valid "use_of_name" in surname');

        $self->_verify_against_params($object, \%params);

    }, 'Tested br_subject_usage_create: object_from_params for person');

}

=head2 br_subject_usage_create_validation_tests

Validation checks for subject

=cut

sub br_subject_usage_create_validation_tests : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my %params = (
            'subject_type'              => 'company',
            'subject'                   => {
                'company'       => 'Mintlab B.V',
                'coc_number'            => '12345678',
                'coc_location_number'   => '545698785412',
                'company_type'          => {
                    code    => 1,
                    label   => 'Eenmanszaak',
                },
                'address_residence'          => {
                    street          => 'Donker Curtiusstraat',
                    street_number          => 7,
                    street_number_letter   => 'a',
                    street_number_suffix   => '521',
                    city            => 'Amsterdam',
                    zipcode         => '1051JL',
                    country         => {
                        label           => 'Nederland',
                        dutch_code      => 6030,
                    }
                }
            },
            authentication  => {
                password        => 'xyzuvw',
            }
        );

        my $bridge      = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        my $object      = $bridge->object_from_params(\%params);

        ### Start validation tests

        ### - Missing foreign_address_line:
        $object->subject->address_residence->country->dutch_code(1234);
        throws_ok(sub { $bridge->save($object); }, qr/missing: foreign_address_line1/, 'Country other than NL: missing address');       

        ### - Missing city
        $object->subject->address_residence->country->dutch_code(6030);
        $object->subject->address_residence->city('');
        throws_ok(sub { $bridge->save($object); }, qr/missing: city/, 'Country is NL: missing city');
        $object->subject->address_residence->city('Amsterdam');


        $object->subject->address_residence->street('');
        $object->subject->address_residence->street_number('');
        throws_ok(sub { $bridge->save($object); }, qr/missing: street_number, street/, 'Country is NL: missing street and number');
        $object->subject->address_residence->street('Donker Curtiusstraat');
        $object->subject->address_residence->street_number('7');

        $bridge->save($object);
        throws_ok(sub { $bridge->save($object); }, qr/invalid: coc_number/, 'No 2 companies with the same credentials');
    }, 'Tested br_subject_usage_create_validation_tests: Diverse profile testing');
}


=head2 stuf_nps_usage_importing_via_bridge

=cut

sub br_subject_usage_remote_import : Tests {
    my $self            = shift;

    my $importparams    = {
        'authentication' => undef,
        'display_name' => "HQ van der Jacobs",
        'external_subscription' => undef,
        'old_subject_identifier' => undef,
        'subject' => {
                     'address_correspondence' => undef,
                     'address_residence' => {
                                              'city' => 'Zkxpbopih',
                                              'country' => {
                                                             'alpha_one' => undef,
                                                             'alpha_two' => undef,
                                                             'code' => undef,
                                                             'dutch_code' => '6030',
                                                             'label' => 'Nederland'
                                                           },
                                              'foreign_address_line1' => undef,
                                              'foreign_address_line2' => undef,
                                              'foreign_address_line3' => undef,
                                              'municipality' => {
                                                                  'dutch_code' => '1332',
                                                                  'label' => 'Testgemeente'
                                                                },
                                              'street' => 'Nwzwbnmpskgsptboh',
                                              'street_number' => '58',
                                              'street_number_letter' => 'Y',
                                              'street_number_suffix' => 'JAKT',
                                              'zipcode' => '8031YR'
                                            },
                     'date_of_birth' => '1930-05-16T00:00:00Z',
                     'date_of_death' => undef,
                     'family_name' => "Jacobs",
                     'first_names' => 'Else',
                     'gender' => 'M',
                     'initials' => 'HQ',
                     'is_local_resident' => undef,
                     'is_secret' => undef,
                     'partner' => {
                                    'address_correspondence' => undef,
                                    'address_residence' => undef,
                                    'date_of_birth' => undef,
                                    'date_of_death' => undef,
                                    'family_name' => 'Fleppenstein',
                                    'first_names' => undef,
                                    'gender' => undef,
                                    'initials' => undef,
                                    'is_local_resident' => undef,
                                    'is_secret' => undef,
                                    'partner' => undef,
                                    'personal_number' => '123304256',
                                    'personal_number_a' => '1385664912',
                                    'place_of_birth' => undef,
                                    'prefix' => 'van de',
                                    'surname' => 'van de Fleppenstein',
                                    'use_of_name' => undef
                                  },
                     'personal_number' => '985658569',
                     'personal_number_a' => '1036295625',
                     'place_of_birth' => undef,
                     'prefix' => 'van der',
                     'surname' => "van der Jacobs",
                     'use_of_name' => 'E'
                   },
        'subject_type' => 'person'
    };


    $zs->zs_transaction_ok(sub {
        my $interface       = $self->_create_nps_interface();

        my $bridge          = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
            remote_search   => 'stuf',
        );

        my $object  = $bridge->remote_import($importparams);

        ok($object, "got imported object");

    }, 'Bridge: "search" and "remote_import" via Centric');

}

sub _verify_against_params {
    my $self        = shift;
    my $object      = shift;
    my $params      = shift;

    for my $key (keys %$params) {
        if (ref($params->{$key}) eq 'HASH') {
            $self->_verify_against_params($object->$key, $params->{$key});
            next;
        }

        ### DateTime fix
        my $value = $object->$key;
        if ($key =~ /date/ && blessed($value) && $value->isa('DateTime')) {
            $value = $value->ymd;
        }

        if ($key eq 'password') {
            ok(!$value, 'Got empty password');
            next;
        }

        is ($value, $params->{$key}, 'Object and param match on key: ' . $key);
    }
}


=head2 Create

=cut

# sub br_subject_usage_create : Tests {
#     my $self        = shift;

#     $zs->zs_transaction_ok(sub {
#         ### USAGE
#         my $subject     = Zaaksysteem::BR::Subject->new(
#             schema          => $schema,
#             log             => $schema->default_resultset_attributes->{log},
#             subject_type    => 'bedrijf',
#         );

#         my $params      = {
#             'handelsnaam'                       => 'Gemeente Zaakstad',
#             'dossiernummer'                     => '25654589',
#             'vestigingsnummer'                  => '123456789012',
#             'vestiging_landcode'                => '6030',
#             'vestiging_straatnaam'              => 'Zaakstraat',
#             'vestiging_huisnummer'              => 44,
#             'vestiging_huisletter'              => 'a',
#             'vestiging_huisnummertoevoeging'    => '1rechts',
#             'vestiging_postcode'                => '1234AB',
#             'vestiging_woonplaats'              => 'Amsterdam',

#             'password'                          => 'Test123',
#         };

#         my $rv          = $subject->save($params);

#         ### END USAGE

#         ok($rv->id, 'Found an ID for created subject');

#         my $company = $schema->resultset('Bedrijf')->find($rv->id);
#         my $auth    = $schema->resultset('BedrijfAuthenticatie')->find({ gegevens_magazijn_id => $rv->id });


#         ok($company, 'Found company in database');
#         ok($auth, 'Found authentication credentials in database');

#         for my $key (keys %$params) {
#             if ($key eq 'password') {
#                 is($auth->$key, $params->{$key}, 'Given key matches entry in database for ' . $key);
#                 next;
#             }

#             is($company->$key, $params->{$key}, 'Given key matches entry in database for ' . $key);
#         }

#         ### Check subject object
#         for my $key (keys %$params) {
#             is($subject->$key, $params->{$key}, 'Given key matches entry in object for ' . $key);
#         }
#     });
# }


=head2 br_subject_utility_functions

Tests for utility functions of L<Zaaksysteem::BR::Subject::Utils>

=cut

sub br_subject_utility_functions : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        ### USAGE
        my $bridge     = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        my $params      = {
            subject_type                        => 'person',
            'subject.personal_number'           => '123456789',
            'subject.surname'                   => 'Fritsie',
            'subject.address_residence.street'  => 'donstreet',
        };

        # diag(explain($bridge->map_search_params($params, 'person')));

        is_deeply(
            $bridge->map_search_params($params, 'person'),
            {
                subject => {
                    'burgerservicenummer'   => '123456789',
                    'naamgebruik'           => 'Fritsie',
                    'address_residence'     => {
                        straatnaam  => 'donstreet',
                    }
                },
                subject_type => 'person'
            },
            'Got correct params for map_search_params'
        );
    }, 'Tested: map_search_params');
}

=head2 br_subject_authentication_functions

=cut

sub br_subject_authentication_functions : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        ### Generate a few identical companies;
        my @companies;
        push(@companies, $zs->create_bedrijf_ok(handelsnaam => 'Mintlab B.V.', dossiernummer => 37103637)) for 1..4;

        for my $company (@companies) {
            my $auth = Zaaksysteem::Object::Types::Authentication->new(
                entity_type => 'company',
                password => 'abascd',
            );

            $auth->find_or_generate_new_password(row => $company);
        }

        my $auth = Zaaksysteem::Object::Types::Authentication->new(
            entity_type => 'company',
            password    => 'abascd',
        );

        my $obj  = $auth->find_or_generate_new_password(row => $companies[3]);


        is($obj->username, '371036373', 'Got incremented username');

        ## Check password
        my $ppr = Authen::Passphrase->from_rfc2307($obj->hashed_password);

        ok($ppr->match('abascd'), 'Got matching password with hashed one');

    }, 'Tested: authentication function');
}

=head2 br_subject_api_v1_reader

Tests for private functions of L<Zaaksysteem::BR::Subject>

=cut

sub br_subject_api_v1_reader : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        $zs->create_bedrijf_ok(handelsnaam => 'Mintlab B.V.');

        my $serializer = Zaaksysteem::API::v1::Serializer->new(
            encoding_format => 'JSON'
        );

        my $bridge     = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        ### Retrieve plain subject
        my $subjects    = $bridge->search(
            {
                subject_type  => 'company',
                company       => '%Mintlab B.V.%',
            },
        );

        my $subject     = $subjects->first;

        # diag($subject);

        # diag(explain($serializer->read($subject)));


    }, 'Tested: subject reader');
}

=head2 br_subject_bulk

Create a LOT of subjects

=cut

sub br_subject_bulk : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $interface = $zs->create_interface_ok();

        my %params = (
            'subject_type'              => 'company',
            'subject'                   => {
                'company'       => 'Mintlab B.V.',
                'coc_number'            => '12345678',
                'coc_location_number'   => '545698785412',
                'company_type'          => {
                    code    => 1,
                    label   => 'Eenmanszaak',
                },
                'address_residence'          => {
                    street          => 'Donker Curtiusstraat',
                    street_number          => 7,
                    street_number_letter   => 'a',
                    street_number_suffix   => '521',
                    city            => 'Amsterdam',
                    zipcode         => '1051JL',
                    country         => {
                        label           => 'Nederland',
                        dutch_code      => 6030,
                    }
                }
            }
        );

        my $bridge      = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        ### When just having some params, retrieve the Subject object from the given params
        $zs->num_queries_ok(
            sub {
                my $coc_number;
                my $interface_count = 24;
                for (1..100) {
                    my $object      = $bridge->object_from_params(\%params);

                    ### We need to increase the coc_number: it is unique
                    $coc_number  = $object->subject->coc_number unless $coc_number;
                    $object->subject->coc_number(++$coc_number);
                    my $newobject = $bridge->save($object);

                    ### Create objectsubscription
                    $schema->resultset('ObjectSubscription')->create(
                        {
                            local_table     => 'Bedrijf',
                            local_id        => $newobject->subject->_table_id,
                            external_id     => $interface_count++,
                            interface_id    => $interface->id,
                        }
                    );
                }
            },
            500
        );
        is($schema->resultset('Bedrijf')->count, 100, 'Created 100 companies');

        my (@objects, $subjects);
        $zs->num_queries_ok(
            sub {
                $subjects                = $bridge->search({subject_type => 'company', company => 'Mintlab B.V.'});
                while (my $row = $subjects->next) {
                    push(@objects, $row);
                }
            },
            201
        );
        is(@objects, 100, 'Retrieved a 100 companies inefficient');

        $zs->num_queries_ok(
            sub {
                @objects = ();
                $subjects                = $subjects->search({subject_type => 'company', company => 'Mintlab B.V.'}, { rows => 100 });
                while (my $row = $subjects->next) {
                    push(@objects, $row);
                }
            },
            3
        );
        is(@objects, 100, 'Retrieved a 100 companies efficient');

        ### Check if person has a subscription
        my ($first) = $objects[0];

        ok($first->external_subscription->external_identifier, "Got a external identifier");
    }, 'Tested br_subject_usage_create: Create and retrieve 100 companies');

    $zs->zs_transaction_ok(sub {
        my $interface = $zs->create_interface_ok();

        my %params = (
            subject_type    => 'person',
            subject         => {
                'personal_number'           => '123456789',
                'personal_number_a'         => '1987654321',
                'initials'                  => 'D.',
                'first_names'               => 'Don',
                'family_name'               => 'Fuego',
                'surname'                   => 'The Fuego',
                'prefix'                    => 'The',
                'gender'                    => 'M',
                'date_of_birth'             => '1982-06-05',
                'use_of_name'               => 'E',
                'address_residence'         => {
                    street          => 'Muiderstraat',
                    street_number          => 42,
                    street_number_letter   => 'a',
                    street_number_suffix   => '521',
                    city            => 'Amsterdam',
                    zipcode         => '1011PZ',
                    country         => {
                        label           => 'Nederland',
                        dutch_code      => 6030,
                    }
                },
                'address_correspondence'         => {
                    street          => 'Donkerstraat',
                    street_number          => 1,
                    street_number_letter   => 'b',
                    street_number_suffix   => '522',
                    city            => 'Donkerdam',
                    zipcode         => '1011PA',
                    country         => {
                        label           => 'Nederland',
                        dutch_code      => 6030,
                    }
                }
            },
        );

        my $bridge      = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
        );

        ### When just having some params, retrieve the Subject object from the given params
        $zs->num_queries_ok(
            sub {
                my $interface_count = 24;
                for (1..100) {
                    my $object      = $bridge->object_from_params(\%params);
                    my $newobject   = $bridge->save($object);

                    ### Create objectsubscription
                    $schema->resultset('ObjectSubscription')->create(
                        {
                            local_table     => 'NatuurlijkPersoon',
                            local_id        => $newobject->subject->_table_id,
                            external_id     => $interface_count++,
                            interface_id    => $interface->id,
                        }
                    );
                }
            },
            1400
        );
        is($schema->resultset('NatuurlijkPersoon')->count, 100, 'Created 100 persons');

        my (@objects, $subjects);
        $zs->num_queries_ok(
            sub {
                $subjects                = $bridge->search({subject_type => 'person', personal_number => '123456789'});
                while (my $row = $subjects->next) {
                    push(@objects, $row);
                }
            },
            201
        );
        is(@objects, 100, 'Retrieved a 100 persons inefficient');

        $zs->num_queries_ok(
            sub {
                @objects = ();
                $subjects                = $subjects->search({subject_type => 'person', personal_number => '123456789'}, { rows => 100 });
                while (my $row = $subjects->next) {
                    push(@objects, $row);
                }
            },
            3
        );
        is(@objects, 100, 'Retrieved a 100 persons efficient');

        ### Check if person has a subscription
        my ($first) = $objects[0];

        ok($first->external_subscription->external_identifier, "Got a external identifier");

    }, 'Tested br_subject_usage_create: Create and retrieve 100 persons');
}

=head1 PRIVATE METHODS

=cut

sub _verify_company_object {
    my ($self, $subject) = @_;

    my $adr_map = {
        city    => 'Amsterdam',
        street_number  => 7,
        street  => 'Donker Curtiusstraat',
        zipcode => '1051JL',
        street_number_suffix => '521',
        street_number_letter => 'a',
    };

    if ($subject->subject->address_correspondence) {
        $self->_verify_address_object($subject->subject->address_correspondence, $adr_map);
    }

    if ($subject->subject->address_residence) {
        $self->_verify_address_object($subject->subject->address_residence, $adr_map);
    }

    like($subject->subject->coc_number, qr/^\d+$/, 'Got a valid "coc_number"');
    like($subject->subject->coc_location_number, qr/^\d+$/, 'Got a valid "coc_location_number"');
    ok($subject->subject->company, 'Got a company: ' . $subject->subject->company);
    ok($subject->subject->company_type, 'Got a company_type: ' . $subject->subject->company_type->label);
}

sub _verify_address_object {
    my ($self, $address, $map) = @_;

    is ($address->$_, $map->{$_}, "Found correct $_: " . $address->$_) for keys %$map;
    is ($address->country->label, 'Nederland', 'Found correct country label: ' . $address->country->label);
    is ($address->country->dutch_code, '6030', 'Found correct country dutch_code: ' . $address->country->dutch_code);
}

sub _verify_person_object {
    my ($self, $subject) = @_;

    my $adr_map = {
        residence => {
            city    => 'Amsterdam',
            street_number  => 42,
            street  => 'Muiderstraat',
            zipcode => '1011PZ',
            street_number_suffix => '521',
            street_number_letter => 'a',
        },
        correspondence => {
            street          => 'Donkerstraat',
            street_number          => 1,
            street_number_letter   => 'b',
            street_number_suffix   => '522',
            city            => 'Donkerdam',
            zipcode         => '1011PA',
        },
    };

    ok($subject->subject->$_, "Got a $_: " . $subject->subject->$_) for qw/family_name initials first_names personal_number surname gender personal_number_a prefix/;

    if ($subject->subject->address_correspondence) {
        $self->_verify_address_object($subject->subject->address_correspondence, $adr_map->{correspondence});
    }

    if ($subject->subject->address_residence) {
        $self->_verify_address_object($subject->subject->address_residence, $adr_map->{residence});
    }
}

sub _verify_employee_object {
    my ($self, $subject) = @_;

    ok($subject->subject->$_, "Got a $_: " . $subject->subject->$_) for qw/surname display_name first_names initials/;
}

sub _create_nps_interface {
    my $self            = shift;
    my $params          = shift || {};

    $zs->create_named_interface_ok(
        {
            module              => 'stufconfig',
            name                => 'STUF PRS Parsing',
            interface_config    => {
                stuf_supplier           => 'centric',
                synchronization_type    => 'question',
                gemeentecode            => '363',
                mk_sender               => 'ZSNL',
                mk_ontvanger            => 'CGS',
                gbav_applicatie         => 'CML',
                mk_ontvanger_afnemer    => 'CMODIS',
                mk_async_url            => 'https://localhost/stuf/async',
                mk_sync_url             => 'https://localhost/stuf/sync',
                gbav_pink_url           => 'https://localhost/stuf/pink',
                gbav_search             => 1,
                mk_spoof                => 1,
            }
        },
    );

    return $zs->create_named_interface_ok(
        {
            module          => 'stufnps',
            name            => 'STUF NPS Parsing',
            active          => 1,
        }
    );
}



1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

