package TestFor::General::StUF::0312;
use base qw(Test::Class);

use Moose;
use TestSetup;
use File::Spec::Functions qw(catfile);

sub _is_xml_data_ok {
    my ($data, $expect, $msg) = @_;
    my $ok = is_deeply($data, $expect, $msg);
    if (!$ok) {
        diag explain { expected => $data, got => $expect };
    }
    return $ok;
}

sub zs_stuf_0312_aanvraag : Tests {

    $zs->txn_ok(
        sub {
            my $stuf = $zs->stuf_0312;

            my $xml = $zs->open_stuf_xml('0312', 'aanvraag_bedrijf.xml');
            my $data = $stuf->aanvraag_omgevingsloket('READER', $xml);

            my $expect = {
                'stuurgegevens' => {
                    'berichtcode' => 'Di01',
                    'functie'     => 'AanbiedenAanvraag',
                    'ontvanger'   => {
                        'administratie' => 'omvvergunning',
                        'applicatie'    => 'Gemeente zztestgemeente18',
                        'organisatie'   => '012345678'
                    },
                    'referentienummer' => '122485',
                    'tijdstipBericht'  => '20140411164437',
                    'zender'           => {
                        'administratie' => 'omvvergunning',
                        'applicatie'    => 'OLO',
                        'organisatie'   => 'VROM'
                    }
                },
                'update' => {
                    'object' => {
                        'aanvraagGegevens' => {
                            'aanvraagNaam' => {
                                '_'     => 'test3465758',
                                'exact' => 1
                            },
                            'aanvraagProcedure' => {
                                '_'     => 'Reguliere procedure',
                                'exact' => 1
                            },
                            'aanvraagStatus' => {
                                '_'     => 'In behandeling',
                                'exact' => 1
                            },
                            'aanvraagdatum' => {
                                '_' => bless(
                                    {
                                        '_e'   => [0],
                                        '_es'  => '+',
                                        '_m'   => [20140408],
                                        'sign' => '+'
                                    },
                                    'Math::BigFloat'
                                ),
                                'exact'               => 1,
                                'indOnvolledigeDatum' => 'V'
                            },
                            'aanvraagnummer' => {
                                '_'     => '33991',
                                'exact' => 1
                            },
                            'gedeeltelijkGoedkeurenGewenst' => {
                                '_'     => 'nee',
                                'exact' => 1
                            },
                            'gefaseerdIndienen' => {
                                '_'     => 'niet',
                                'exact' => 1
                            },
                            'landelijkeFormulierVersie' => {
                                '_'     => '2013.01',
                                'exact' => 1
                            },
                            'omschrijvingNietInTeDienenBijlagen' => {
                                '_'     => 'nmnmn',
                                'exact' => 1
                            },
                            'omschrijvingUitgesteldeBijlagen' => {
                                '_'     => 'jknmn',
                                'exact' => 1
                            },
                            'persoonsgegevensVrijgeven' => {
                                '_'     => 'nee',
                                'exact' => 1
                            },
                            'plichtStatus' => {
                                '_'     => 'Vergunningsplicht',
                                'exact' => 1
                            },
                            'projectKosten' => {
                                '_'     => 4567,
                                'exact' => 1
                            },
                            'projectOmschrijving' => {
                                '_'     => 'jkjkjkjkjkjk',
                                'exact' => 1
                            },
                            'referentieCodeAanvrager' => {
                                '_'     => 'mjkj',
                                'exact' => 1
                            },
                            'toelichtingBijIndiening' => {
                                '_'     => '-',
                                'exact' => 1
                            },
                            'uitgesteldeAanleveringBijlagen' => {
                                '_'     => 'ja',
                                'exact' => 1
                            }
                        },
                        'heeftBijlage' => [
                            {
                                'gerelateerde' => {
                                    'bestandsnaam' => {
                                        '_' =>
                                            '/33991/basisset_aanvraag/33991_1396945003501_Bouwtekening.pdf',
                                        'exact' => 1
                                    },
                                    'bestandsnaamOrigineel' => {
                                        '_'     => 'Bouwtekening.pdf',
                                        'exact' => 1
                                    },
                                    'documenttype-omschrijving-generiek' => [
                                        {
                                            '_'     => 'Tekening (technisch)',
                                            'exact' => 1
                                        }
                                    ],
                                    'formeleBijlage' => {
                                        '_'     => 'ja',
                                        'exact' => 1
                                    },
                                    'isEen' => {
                                        'gerelateerde' => {
                                            'creatiedatum' => {
                                                '_' => bless(
                                                    {
                                                        '_e'   => [0],
                                                        '_es'  => '+',
                                                        '_m'   => [20140408],
                                                        'sign' => '+'
                                                    },
                                                    'Math::BigFloat'
                                                ),
                                                'exact'               => 1,
                                                'indOnvolledigeDatum' => 'V'
                                            },
                                            'identificatie' => {
                                                '_'     => '1120719',
                                                'exact' => 1
                                            },
                                            'status' => {
                                                '_'     => 'InBehandeling',
                                                'exact' => 1
                                            },
                                            'titel' => {
                                                '_'     => 'Bouwtekening_pdf',
                                                'exact' => 1
                                            },
                                            'versie' => {
                                                '_'     => '1',
                                                'exact' => 1
                                            },
                                            'verzenddatum' => {
                                                '_' => bless(
                                                    {
                                                        '_e'   => [0],
                                                        '_es'  => '+',
                                                        '_m'   => [20140408],
                                                        'sign' => '+'
                                                    },
                                                    'Math::BigFloat'
                                                ),
                                                'exact'               => 1,
                                                'indOnvolledigeDatum' => 'V'
                                            }
                                        }
                                    },
                                    'soort' => [
                                        {
                                            '_' =>
                                                'Plattegronden, doorsneden en detailtekeningen bouwen',
                                            'exact' => 1
                                        }
                                    ],
                                    'toegevoegdDoor' => {
                                        '_'     => 'Omgevingsloket',
                                        'exact' => 1
                                    },
                                    'valtOnderGeheimhouding' => {
                                        '_'     => 'nee',
                                        'exact' => 1
                                    },
                                    'werkzaamheid' => [
                                        {
                                            'volgnummer' => {
                                                '_'     => 1120713,
                                                'exact' => 1
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                'gerelateerde' => {
                                    'bestandsnaam' => {
                                        '_' =>
                                            '/33991/basisset_aanvraag/33991_1397227477521_papierenformulier.pdf',
                                        'exact' => 1
                                    },
                                    'isEen' => {
                                        'gerelateerde' => {
                                            'formaat' => {
                                                '_'     => 'PDF',
                                                'exact' => 1
                                            },
                                            'titel' => {
                                                '_'     => 'test3465758',
                                                'exact' => 1
                                            },
                                            'verzenddatum' => {
                                                '_' => bless(
                                                    {
                                                        '_e'   => [0],
                                                        '_es'  => '+',
                                                        '_m'   => [20140408],
                                                        'sign' => '+'
                                                    },
                                                    'Math::BigFloat'
                                                ),
                                                'exact'               => 1,
                                                'indOnvolledigeDatum' => 'V'
                                            }
                                        }
                                    },
                                    'opmerkingen' => {
                                        '_'     => '-',
                                        'exact' => 1
                                    },
                                    'soort' => [
                                        {
                                            '_'     => 'Aanvraagdocument',
                                            'exact' => 1
                                        }
                                    ]
                                }
                            },
                            {
                                'gerelateerde' => {
                                    'bestandsnaam' => {
                                        '_' =>
                                            '/33991/basisset_aanvraag/33991_1397227477526_publiceerbareaanvraag.pdf',
                                        'exact' => 1
                                    },
                                    'isEen' => {
                                        'gerelateerde' => {
                                            'formaat' => {
                                                '_'     => 'PDF',
                                                'exact' => 1
                                            },
                                            'titel' => {
                                                '_'     => 'test3465758',
                                                'exact' => 1
                                            },
                                            'verzenddatum' => {
                                                '_' => bless(
                                                    {
                                                        '_e'   => [0],
                                                        '_es'  => '+',
                                                        '_m'   => [20140408],
                                                        'sign' => '+'
                                                    },
                                                    'Math::BigFloat'
                                                ),
                                                'exact'               => 1,
                                                'indOnvolledigeDatum' => 'V'
                                            }
                                        }
                                    },
                                    'opmerkingen' => {
                                        '_'     => '-',
                                        'exact' => 1
                                    },
                                    'soort' => [
                                        {
                                            '_' =>
                                                'Aanvraagdocument publiceerbaar',
                                            'exact' => 1
                                        }
                                    ]
                                }
                            }
                        ],
                        'isAangevraagdDoor' => {
                            'gerelateerde' => {
                                'cho_kvkNummer' => [
                                    {
                                        'cho_natuurlijkPersoon' => [
                                            {
                                                'vestiging' => {
                                                    'handelsnaam' => {
                                                        '_' => 'Mintlab B.V.',
                                                        'exact' => 1
                                                    },
                                                    'heeftAlsContactPersoon' =>
                                                        {
                                                        'functionarisType' => {
                                                            '_' =>
                                                                'Verzamenlaar',
                                                            'exact' => 1
                                                        },
                                                        'gerelateerde' => {
                                                            'natuurlijkPersoon'
                                                                => {
                                                                'cho_verblijfsadres'
                                                                    => [
                                                                    {
                                                                        'verblijfsadres'
                                                                            => {
                                                                            }
                                                                    }
                                                                    ],
                                                                'geslachtsaanduiding'
                                                                    => {
                                                                    '_' => 'M',
                                                                    'exact' => 1
                                                                    },
                                                                'geslachtsnaam'
                                                                    => {
                                                                    '_' =>
                                                                        'Moen',
                                                                    'exact' => 1
                                                                    },
                                                                'voorletters' =>
                                                                    {
                                                                    '_' => 'PP',
                                                                    'exact' => 1
                                                                    }
                                                                }
                                                        }
                                                        },
                                                    'statutaireNaam' => {
                                                        '_' => 'Mintlab B.V.',
                                                        'exact' => 1
                                                    },
                                                    'sub.emailadres' => {
                                                        '_' =>
                                                            'servicedesk@mintlab.nl',
                                                        'exact' => 1
                                                    },
                                                    'sub.telefoonnummer' => {
                                                        '_'     => '0207370005',
                                                        'exact' => 1
                                                    },
                                                    'verblijfsadres' => {
                                                        'aoa.huisnummer' => {
                                                            '_'     => 7,
                                                            'exact' => 1
                                                        },
                                                        'aoa.postcode' => {
                                                            '_'     => '1051JL',
                                                            'exact' => 1
                                                        },
                                                        'gor.straatnaam' => {
                                                            '_' =>
                                                                'Donker Curtiusstraat',
                                                            'exact' => 1
                                                        },
                                                        'wpl.woonplaatsNaam' =>
                                                            {
                                                            '_' => 'Amsterdam',
                                                            'exact' => 1
                                                            }
                                                    },
                                                    'vestigingsNummer' => {
                                                        '_' => '999888555418',
                                                        'exact' => 1
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        },
                        'isVoor' => {
                            'gerelateerde' => {
                                'seq_isKadastraleOnroerendeZaak' => [
                                    {
                                        'eigendomsSituatie' => {
                                            '_'     => 'Erfpachter',
                                            'exact' => 1
                                        },
                                        'isAdres' => {
                                            'gerelateerde' => {
                                                'adresAanduidingGrp' => {
                                                    'aoa.huisnummer' => {
                                                        '_'     => 18,
                                                        'exact' => 1
                                                    },
                                                    'aoa.postcode' => {
                                                        '_'     => '8888XX',
                                                        'exact' => 1
                                                    },
                                                    'gor.straatnaam' => {
                                                        '_'     => 'Teststraat',
                                                        'exact' => 1
                                                    },
                                                    'wpl.woonplaatsNaam' => {
                                                        '_' => 'TSTPLAATS 18',
                                                        'exact' => 1
                                                    }
                                                }
                                            }
                                        },
                                        'locatieAanduiding' => {
                                            '_'     => 'Adresseerbaar',
                                            'exact' => 1
                                        },
                                        'meerderePercelen' => {
                                            '_'     => 'nee',
                                            'exact' => 1
                                        },
                                        'omschrijvingLocatie' => {
                                            '_'     => 'Geen',
                                            'exact' => 1
                                        }
                                    }
                                ]
                            }
                        },
                        'werkzaamheid' => [
                            {
                                'onderdeel' => [
                                    {
                                        'geschatteKosten' => {
                                            '_'     => 20000,
                                            'exact' => 1
                                        },
                                        'onderdeelBouwen' => {
                                            'BOUWENDevergunning' => {
                                                'Bouwvorm' => {
                                                    '_' =>
                                                        'Het wordt gedeeltelijk vervangen',
                                                    'exact' => 1
                                                },
                                                'EerdereBouwvergunning' => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                },
                                                'ToelichtingBouwwerkzaamheden'
                                                    => {
                                                    '_' =>
                                                        'Er wordt een deel van de bestaande woning gesloopt om de aanbouw te kunnne bouwen',
                                                    'exact' => 1
                                                    }
                                            },
                                            'BOUWENGebruik' => {
                                                'GebrOppWonen' => {
                                                    '_'     => 130,
                                                    'exact' => 1
                                                },
                                                'HuidigGebruikBouwwerk' => [
                                                    {
                                                        '_'     => 'Wonen',
                                                        'exact' => 1
                                                    }
                                                ],
                                                'ToekomstigGebruikBouwwerk' => [
                                                    {
                                                        '_'     => 'Wonen',
                                                        'exact' => 1
                                                    }
                                                ],
                                                'VlOppVerblWonen' => {
                                                    '_'     => 130,
                                                    'exact' => 1
                                                }
                                            },
                                            'BOUWENSeizoensgebondenentijdelijk'
                                                => {
                                                'SeizoensgebondenBouwwerk' => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                },
                                                'TijdelijkBouwwerk' => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                }
                                                },
                                            'BOUWENUiterlijkbouwwerkenkosten' =>
                                                {
                                                'BouwplanMondToelichten' => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                },
                                                'onderdeel' => [
                                                    {
                                                        'kleur' => {
                                                            '_'     => 'Grijs',
                                                            'exact' => 1
                                                        },
                                                        'materiaal' => {
                                                            '_' => 'Baksteen',
                                                            'exact' => 1
                                                        },
                                                        'soortOnderdeel' => {
                                                            '_'     => 'Gevels',
                                                            'exact' => 1
                                                        }
                                                    },
                                                    {
                                                        'kleur' => {
                                                            '_'     => 'Wit',
                                                            'exact' => 1
                                                        },
                                                        'materiaal' => {
                                                            '_'     => 'Hout',
                                                            'exact' => 1
                                                        },
                                                        'soortOnderdeel' => {
                                                            '_' => 'Kozijnen',
                                                            'exact' => 1
                                                        }
                                                    },
                                                    {
                                                        'kleur' => {
                                                            '_'     => 'Grijs',
                                                            'exact' => 1
                                                        },
                                                        'materiaal' => {
                                                            '_' => 'Kunstof',
                                                            'exact' => 1
                                                        },
                                                        'soortOnderdeel' => {
                                                            '_' =>
                                                                'Dakgoten en boeidelen',
                                                            'exact' => 1
                                                        }
                                                    },
                                                    {
                                                        'kleur' => {
                                                            '_'     => 'Grijs',
                                                            'exact' => 1
                                                        },
                                                        'materiaal' => {
                                                            '_' => 'Dakpannen',
                                                            'exact' => 1
                                                        },
                                                        'soortOnderdeel' => {
                                                            '_' =>
                                                                'Dakbedekking',
                                                            'exact' => 1
                                                        }
                                                    }
                                                ]
                                                },
                                            'BOUWENVeranderingafmetingen' => {
                                                'BrutoInhoudBouwwerkNa' => {
                                                    '_'     => 120,
                                                    'exact' => 1
                                                },
                                                'BrutoInhoudBouwwerkVoor' => {
                                                    '_'     => 100,
                                                    'exact' => 1
                                                },
                                                'BrutoOppervlakteBouwwerkNa' =>
                                                    {
                                                    '_'     => 25,
                                                    'exact' => 1
                                                    },
                                                'BrutoOppervlakteBouwwerkVoor'
                                                    => {
                                                    '_'     => 20,
                                                    'exact' => 1
                                                    },
                                                'VeranderingBebouwdeOppervlakteTerrein'
                                                    => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                    },
                                                'VeranderingBrutoInhoudBouwwerk'
                                                    => {
                                                    '_'     => 'Ja',
                                                    'exact' => 1
                                                    },
                                                'VeranderingBrutoOppervlakteBouwwerk'
                                                    => {
                                                    '_'     => 'Ja',
                                                    'exact' => 1
                                                    }
                                            },
                                            'BOUWENWaargaatubouwen' => {
                                                'KeuzeObject' => {
                                                    '_'     => 'Op het terrein',
                                                    'exact' => 1
                                                }
                                            }
                                        },
                                        'plichtStatus' => {
                                            '_'     => 'Vergunningsplicht',
                                            'exact' => 1
                                        },
                                        'preciseringLocatie' => {
                                            '_'     => 'Terrein',
                                            'exact' => 1
                                        },
                                        'soort' => {
                                            '_'     => 'Bouwen',
                                            'exact' => 1
                                        },
                                        'toelichting' => {
                                            '_' =>
                                                'Bijbehorend bouwwerk bouwen - Bouwen - Terrein',
                                            'exact' => 1
                                        },
                                        'volgnummer' => {
                                            '_'     => 1120715,
                                            'exact' => 1
                                        }
                                    }
                                ],
                                'soort' => {
                                    '_'     => 'Bijbehorend bouwwerk bouwen',
                                    'exact' => 1
                                },
                                'volgnummer' => {
                                    '_'     => 1120713,
                                    'exact' => 1
                                }
                            }
                        ]
                    },
                    'parameters' => {
                        'indicatorOvername' => 'V',
                        'mutatiesoort'      => 'T'
                    }
                }
            };

            _is_xml_data_ok($data, $expect, "Aanvraag NNP");

        },
        "Omgevingsloket aanvraag: NNP",
    );

    $zs->txn_ok(
        sub {
            my $stuf = $zs->stuf_0312;

            my $xml
                = $zs->open_stuf_xml('0312', 'aanvraag_natuurlijk_persoon.xml');
            my $data = $stuf->aanvraag_omgevingsloket('READER', $xml);

            my $expect = {
                'stuurgegevens' => {
                    'berichtcode' => 'Di01',
                    'functie'     => 'AanbiedenAanvraag',
                    'ontvanger'   => {
                        'administratie' => 'omvvergunning',
                        'applicatie'    => 'Gemeente zztestgemeente18',
                        'organisatie'   => '012345678'
                    },
                    'referentienummer' => '127641',
                    'tijdstipBericht'  => '20140519165104',
                    'zender'           => {
                        'administratie' => 'omvvergunning',
                        'applicatie'    => 'OLO',
                        'organisatie'   => 'VROM'
                    }
                },
                'update' => {
                    'object' => {
                        'aanvraagGegevens' => {
                            'aanvraagNaam' => {
                                '_'     => 'Uitbouw woonhuis',
                                'exact' => 1
                            },
                            'aanvraagProcedure' => {
                                '_'     => 'Reguliere procedure',
                                'exact' => 1
                            },
                            'aanvraagStatus' => {
                                '_'     => 'In behandeling',
                                'exact' => 1
                            },
                            'aanvraagdatum' => {
                                '_' => bless(
                                    {
                                        '_e'   => [0],
                                        '_es'  => '+',
                                        '_m'   => [20140519],
                                        'sign' => '+'
                                    },
                                    'Math::BigFloat'
                                ),
                                'exact'               => 1,
                                'indOnvolledigeDatum' => 'V'
                            },
                            'aanvraagnummer' => {
                                '_'     => '34573',
                                'exact' => 1
                            },
                            'gedeeltelijkGoedkeurenGewenst' => {
                                '_'     => 'nee',
                                'exact' => 1
                            },
                            'gefaseerdIndienen' => {
                                '_'     => 'niet',
                                'exact' => 1
                            },
                            'landelijkeFormulierVersie' => {
                                '_'     => '2013.01',
                                'exact' => 1
                            },
                            'omschrijvingNietInTeDienenBijlagen' => {
                                '_'     => 'sdfsdf',
                                'exact' => 1
                            },
                            'omschrijvingUitgesteldeBijlagen' => {
                                '_'     => 'sddsf',
                                'exact' => 1
                            },
                            'persoonsgegevensVrijgeven' => {
                                '_'     => 'ja',
                                'exact' => 1
                            },
                            'plichtStatus' => {
                                '_'     => 'Vergunningsplicht',
                                'exact' => 1
                            },
                            'projectKosten' => {
                                '_'     => 25000,
                                'exact' => 1
                            },
                            'projectOmschrijving' => {
                                '_' =>
                                    'We gaan een uitbouw laten bouwen aan ons bestaande huis',
                                'exact' => 1
                            },
                            'referentieCodeAanvrager' => {
                                '_'     => '124',
                                'exact' => 1
                            },
                            'toelichtingBijIndiening' => {
                                '_'     => '-',
                                'exact' => 1
                            },
                            'uitgesteldeAanleveringBijlagen' => {
                                '_'     => 'ja',
                                'exact' => 1
                            }
                        },
                        'heeftBijlage' => [
                            {
                                'gerelateerde' => {
                                    'bestandsnaam' => {
                                        '_' =>
                                            '/34573/basisset_aanvraag/34573_1400511010240_Aanvraagformulier_Logius_1.6.1.pdf',
                                        'exact' => 1
                                    },
                                    'bestandsnaamOrigineel' => {
                                        '_' =>
                                            'Aanvraagformulier_Logius_1.6.1.pdf',
                                        'exact' => 1
                                    },
                                    'documenttype-omschrijving-generiek' => [
                                        {
                                            '_'     => 'Rapport',
                                            'exact' => 1
                                        }
                                    ],
                                    'formeleBijlage' => {
                                        '_'     => 'ja',
                                        'exact' => 1
                                    },
                                    'isEen' => {
                                        'gerelateerde' => {
                                            'creatiedatum' => {
                                                '_' => bless(
                                                    {
                                                        '_e'   => [0],
                                                        '_es'  => '+',
                                                        '_m'   => [20140519],
                                                        'sign' => '+'
                                                    },
                                                    'Math::BigFloat'
                                                ),
                                                'exact'               => 1,
                                                'indOnvolledigeDatum' => 'V'
                                            },
                                            'identificatie' => {
                                                '_'     => '1134261',
                                                'exact' => 1
                                            },
                                            'status' => {
                                                '_'     => 'InBehandeling',
                                                'exact' => 1
                                            },
                                            'titel' => {
                                                '_' =>
                                                    'Aanvraagformulier_Logius_1_6_1_pdf',
                                                'exact' => 1
                                            },
                                            'versie' => {
                                                '_'     => '1',
                                                'exact' => 1
                                            },
                                            'verzenddatum' => {
                                                '_' => bless(
                                                    {
                                                        '_e'   => [0],
                                                        '_es'  => '+',
                                                        '_m'   => [20140519],
                                                        'sign' => '+'
                                                    },
                                                    'Math::BigFloat'
                                                ),
                                                'exact'               => 1,
                                                'indOnvolledigeDatum' => 'V'
                                            }
                                        }
                                    },
                                    'soort' => [
                                        {
                                            '_' => 'Constructieve veiligheid',
                                            'exact' => 1
                                        }
                                    ],
                                    'toegevoegdDoor' => {
                                        '_'     => 'Mintlab',
                                        'exact' => 1
                                    },
                                    'valtOnderGeheimhouding' => {
                                        '_'     => 'nee',
                                        'exact' => 1
                                    },
                                    'werkzaamheid' => [
                                        {
                                            'volgnummer' => {
                                                '_'     => 1134235,
                                                'exact' => 1
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                'gerelateerde' => {
                                    'bestandsnaam' => {
                                        '_' =>
                                            '/34573/basisset_aanvraag/34573_1400511064769_papierenformulier.pdf',
                                        'exact' => 1
                                    },
                                    'isEen' => {
                                        'gerelateerde' => {
                                            'formaat' => {
                                                '_'     => 'PDF',
                                                'exact' => 1
                                            },
                                            'titel' => {
                                                '_'     => 'Uitbouw woonhuis',
                                                'exact' => 1
                                            },
                                            'verzenddatum' => {
                                                '_' => bless(
                                                    {
                                                        '_e'   => [0],
                                                        '_es'  => '+',
                                                        '_m'   => [20140519],
                                                        'sign' => '+'
                                                    },
                                                    'Math::BigFloat'
                                                ),
                                                'exact'               => 1,
                                                'indOnvolledigeDatum' => 'V'
                                            }
                                        }
                                    },
                                    'opmerkingen' => {
                                        '_'     => '-',
                                        'exact' => 1
                                    },
                                    'soort' => [
                                        {
                                            '_'     => 'Aanvraagdocument',
                                            'exact' => 1
                                        }
                                    ]
                                }
                            },
                            {
                                'gerelateerde' => {
                                    'bestandsnaam' => {
                                        '_' =>
                                            '/34573/basisset_aanvraag/34573_1400511064774_publiceerbareaanvraag.pdf',
                                        'exact' => 1
                                    },
                                    'isEen' => {
                                        'gerelateerde' => {
                                            'formaat' => {
                                                '_'     => 'PDF',
                                                'exact' => 1
                                            },
                                            'titel' => {
                                                '_'     => 'Uitbouw woonhuis',
                                                'exact' => 1
                                            },
                                            'verzenddatum' => {
                                                '_' => bless(
                                                    {
                                                        '_e'   => [0],
                                                        '_es'  => '+',
                                                        '_m'   => [20140519],
                                                        'sign' => '+'
                                                    },
                                                    'Math::BigFloat'
                                                ),
                                                'exact'               => 1,
                                                'indOnvolledigeDatum' => 'V'
                                            }
                                        }
                                    },
                                    'opmerkingen' => {
                                        '_'     => '-',
                                        'exact' => 1
                                    },
                                    'soort' => [
                                        {
                                            '_' =>
                                                'Aanvraagdocument publiceerbaar',
                                            'exact' => 1
                                        }
                                    ]
                                }
                            }
                        ],
                        'heeftGemachtigde' => {
                            'gerelateerde' => {
                                'cho_kvkNummer' => [
                                    {
                                        'cho_natuurlijkPersoon' => [
                                            {
                                                'vestiging' => {
                                                    'handelsnaam' => {
                                                        '_' => 'Mintlab B.V.',
                                                        'exact' => 1
                                                    },
                                                    'heeftAlsContactPersoon' =>
                                                        {
                                                        'functionarisType' => {
                                                            '_' =>
                                                                'Verzamenlaar',
                                                            'exact' => 1
                                                        },
                                                        'gerelateerde' => {
                                                            'natuurlijkPersoon'
                                                                => {
                                                                'cho_verblijfsadres'
                                                                    => [
                                                                    {
                                                                        'verblijfsadres'
                                                                            => {
                                                                            }
                                                                    }
                                                                    ],
                                                                'geslachtsaanduiding'
                                                                    => {
                                                                    '_' => 'M',
                                                                    'exact' => 1
                                                                    },
                                                                'geslachtsnaam'
                                                                    => {
                                                                    '_' =>
                                                                        'Moen',
                                                                    'exact' => 1
                                                                    },
                                                                'voorletters' =>
                                                                    {
                                                                    '_' => 'PP',
                                                                    'exact' => 1
                                                                    }
                                                                }
                                                        }
                                                        },
                                                    'statutaireNaam' => {
                                                        '_' => 'Mintlab B.V.',
                                                        'exact' => 1
                                                    },
                                                    'sub.emailadres' => {
                                                        '_' =>
                                                            'servicedesk@mintlab.nl',
                                                        'exact' => 1
                                                    },
                                                    'sub.telefoonnummer' => {
                                                        '_'     => '0207370005',
                                                        'exact' => 1
                                                    },
                                                    'verblijfsadres' => {
                                                        'aoa.huisnummer' => {
                                                            '_'     => 7,
                                                            'exact' => 1
                                                        },
                                                        'aoa.postcode' => {
                                                            '_'     => '1051JL',
                                                            'exact' => 1
                                                        },
                                                        'gor.straatnaam' => {
                                                            '_' =>
                                                                'Donker Curtiusstraat',
                                                            'exact' => 1
                                                        },
                                                        'wpl.woonplaatsNaam' =>
                                                            {
                                                            '_' => 'Amsterdam',
                                                            'exact' => 1
                                                            }
                                                    },
                                                    'vestigingsNummer' => {
                                                        '_' => '999888555418',
                                                        'exact' => 1
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        },
                        'isAangevraagdDoor' => {
                            'gerelateerde' => {
                                'cho_kvkNummer' => [
                                    {
                                        'cho_natuurlijkPersoon' => [
                                            {
                                                'natuurlijkPersoon' => {
                                                    'cho_verblijfsadres' => [
                                                        {
                                                            'verblijfsadres' =>
                                                                {
                                                                'aoa.huisnummer'
                                                                    => {
                                                                    '_' => 7,
                                                                    'exact' => 1
                                                                    },
                                                                'aoa.postcode'
                                                                    => {
                                                                    '_' =>
                                                                        '1051JL',
                                                                    'exact' => 1
                                                                    },
                                                                'gor.straatnaam'
                                                                    => {
                                                                    '_' =>
                                                                        'Donker Curtiusstraat',
                                                                    'exact' => 1
                                                                    },
                                                                'wpl.woonplaatsNaam'
                                                                    => {
                                                                    '_' =>
                                                                        'Amsterdam',
                                                                    'exact' => 1
                                                                    }
                                                                }
                                                        }
                                                    ],
                                                    'geslachtsaanduiding' => {
                                                        '_'     => 'M',
                                                        'exact' => 1
                                                    },
                                                    'geslachtsnaam' => {
                                                        '_'     => 'Boer',
                                                        'exact' => 1
                                                    },
                                                    'inp.bsn' => '581238850',
                                                    'sub.emailadres' => {
                                                        '_' =>
                                                            'peter@mintlab.nl',
                                                        'exact' => 1
                                                    },
                                                    'sub.telefoonnummer' => {
                                                        '_'     => '0207370005',
                                                        'exact' => 1
                                                    },
                                                    'voorletters' => {
                                                        '_'     => 'A.',
                                                        'exact' => 1
                                                    },
                                                    'voorvoegselGeslachtsnaam'
                                                        => {
                                                        '_'     => 'de',
                                                        'exact' => 1
                                                        }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        },
                        'isVoor' => {
                            'gerelateerde' => {
                                'seq_isKadastraleOnroerendeZaak' => [
                                    {
                                        'eigendomsSituatie' => {
                                            '_'     => 'Eigenaar',
                                            'exact' => 1
                                        },
                                        'isAdres' => {
                                            'gerelateerde' => {
                                                'adresAanduidingGrp' => {
                                                    'aoa.huisnummer' => {
                                                        '_'     => 18,
                                                        'exact' => 1
                                                    },
                                                    'aoa.postcode' => {
                                                        '_'     => '8888XX',
                                                        'exact' => 1
                                                    },
                                                    'gor.straatnaam' => {
                                                        '_'     => 'Teststraat',
                                                        'exact' => 1
                                                    },
                                                    'wpl.woonplaatsNaam' => {
                                                        '_' => 'TSTPLAATS 18',
                                                        'exact' => 1
                                                    }
                                                }
                                            }
                                        },
                                        'locatieAanduiding' => {
                                            '_'     => 'Adresseerbaar',
                                            'exact' => 1
                                        },
                                        'meerderePercelen' => {
                                            '_'     => 'nee',
                                            'exact' => 1
                                        },
                                        'omschrijvingLocatie' => {
                                            '_'     => 'efsdfsdf',
                                            'exact' => 1
                                        }
                                    }
                                ]
                            }
                        },
                        'werkzaamheid' => [
                            {
                                'onderdeel' => [
                                    {
                                        'geschatteKosten' => {
                                            '_'     => 20000,
                                            'exact' => 1
                                        },
                                        'onderdeelBouwen' => {
                                            'BOUWENDevergunning' => {
                                                'Bouwvorm' => {
                                                    '_' =>
                                                        'Het wordt nieuw geplaatst',
                                                    'exact' => 1
                                                },
                                                'EerdereBouwvergunning' => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                },
                                                'ToelichtingBouwwerkzaamheden'
                                                    => {
                                                    '_'     => 'nnn',
                                                    'exact' => 1
                                                    }
                                            },
                                            'BOUWENGebruik' => {
                                                'GebrOppWonen' => {
                                                    '_'     => 100,
                                                    'exact' => 1
                                                },
                                                'HuidigGebruikBouwwerk' => [
                                                    {
                                                        '_'     => 'Wonen',
                                                        'exact' => 1
                                                    }
                                                ],
                                                'ToekomstigGebruikBouwwerk' => [
                                                    {
                                                        '_'     => 'Wonen',
                                                        'exact' => 1
                                                    }
                                                ],
                                                'VlOppVerblWonen' => {
                                                    '_'     => 100,
                                                    'exact' => 1
                                                }
                                            },
                                            'BOUWENSeizoensgebondenentijdelijk'
                                                => {
                                                'SeizoensgebondenBouwwerk' => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                },
                                                'TijdelijkBouwwerk' => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                }
                                                },
                                            'BOUWENUiterlijkbouwwerkenkosten' =>
                                                {
                                                'BouwplanMondToelichten' => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                },
                                                'OmschrijvingMateriaalKleurgebruik'
                                                    => {
                                                    '_'     => 'sdfsdf',
                                                    'exact' => 1
                                                    },
                                                'onderdeel' => [
                                                    {
                                                        'kleur' => {
                                                            '_'     => 'bruin',
                                                            'exact' => 1
                                                        },
                                                        'materiaal' => {
                                                            '_'     => 'Steen',
                                                            'exact' => 1
                                                        },
                                                        'soortOnderdeel' => {
                                                            '_'     => 'Gevels',
                                                            'exact' => 1
                                                        }
                                                    }
                                                ]
                                                },
                                            'BOUWENVeranderingafmetingen' => {
                                                'VeranderingBebouwdeOppervlakteTerrein'
                                                    => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                    },
                                                'VeranderingBrutoInhoudBouwwerk'
                                                    => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                    },
                                                'VeranderingBrutoOppervlakteBouwwerk'
                                                    => {
                                                    '_'     => 'Nee',
                                                    'exact' => 1
                                                    }
                                            },
                                            'BOUWENWaargaatubouwen' => {
                                                'KeuzeObject' => {
                                                    '_'     => 'Op het terrein',
                                                    'exact' => 1
                                                }
                                            },
                                            'BOUWENZorgwoningen' => {
                                                'SprakeVanZorgwoning' => {
                                                    '_' =>
                                                        'Geen zorgwoning(en)',
                                                    'exact' => 1
                                                }
                                            }
                                        },
                                        'plichtStatus' => {
                                            '_'     => 'Vergunningsplicht',
                                            'exact' => 1
                                        },
                                        'preciseringLocatie' => {
                                            '_'     => 'Terrein',
                                            'exact' => 1
                                        },
                                        'soort' => {
                                            '_'     => 'Bouwen',
                                            'exact' => 1
                                        },
                                        'toelichting' => {
                                            '_' =>
                                                'Bijbehorend bouwwerk bouwen - Bouwen - Terrein',
                                            'exact' => 1
                                        },
                                        'volgnummer' => {
                                            '_'     => 1134237,
                                            'exact' => 1
                                        }
                                    }
                                ],
                                'soort' => {
                                    '_'     => 'Bijbehorend bouwwerk bouwen',
                                    'exact' => 1
                                },
                                'volgnummer' => {
                                    '_'     => 1134235,
                                    'exact' => 1
                                }
                            }
                        ]
                    },
                    'parameters' => {
                        'indicatorOvername' => 'V',
                        'mutatiesoort'      => 'T'
                    }
                }
            };

            _is_xml_data_ok($data, $expect, "Aanvraag NP");

        },
        "Omgevingsloket aanvraag: NP",
    );
}

sub zs_stuf_0312_aanvulling : Tests {
    $zs->txn_ok(
        sub {
            my $stuf = $zs->stuf_0312;

            my $xml = $zs->open_stuf_xml('0312', 'aanvulling_bedrijf.xml');
            my $data = $stuf->aanvulling_omgevingsloket('READER', $xml);
            my $expect = {
                'stuurgegevens' => {
                    'berichtcode' => 'Di01',
                    'functie'     => 'IndienenAanvulling',
                    'ontvanger'   => {
                        'administratie' => 'omvvergunning',
                        'applicatie'    => 'Gemeente Voerendaal',
                        'organisatie'   => 'Gemeente Voerendaal'
                    },
                    'referentienummer' => '156819',
                    'tijdstipBericht'  => '20141215150701',
                    'zender'           => {
                        'administratie' => 'omvvergunning',
                        'applicatie'    => 'OLO',
                        'organisatie'   => 'VROM'
                    }
                },
                'update' => {
                    'object' => {
                        'aanvraagGegevens' => {
                            'aanvraagnummer' => {
                                '_'     => '38433',
                                'exact' => 1
                            }
                        },
                        'heeftBijlage' => [
                            {
                                'gerelateerde' => {
                                    'bestandsnaam' => {
                                        '_' =>
                                            '/38433/basisset_aanvraag/38433_1418652325424_document_koppelingen_config.png',
                                        'exact' => 1
                                    },
                                    'bestandsnaamOrigineel' => {
                                        '_' =>
                                            'document_koppelingen_config.png',
                                        'exact' => 1
                                    },
                                    'documenttype-omschrijving-generiek' => [
                                        {
                                            '_'     => 'Beschrijving',
                                            'exact' => 1
                                        }
                                    ],
                                    'formeleBijlage' => {
                                        '_'     => 'ja',
                                        'exact' => 1
                                    },
                                    'isEen' => {
                                        'gerelateerde' => {
                                            'auteur' => {
                                                '_'     => 'ik',
                                                'exact' => 1
                                            },
                                            'creatiedatum' => {
                                                '_' => bless(
                                                    {
                                                        '_e'   => [0],
                                                        '_es'  => '+',
                                                        '_m'   => [20141215],
                                                        'sign' => '+'
                                                    },
                                                    'Math::BigFloat'
                                                ),
                                                'exact'               => 1,
                                                'indOnvolledigeDatum' => 'V'
                                            },
                                            'identificatie' => {
                                                '_'     => '1241737',
                                                'exact' => 1
                                            },
                                            'status' => {
                                                '_'     => 'Aanvulling',
                                                'exact' => 1
                                            },
                                            'titel' => {
                                                '_' =>
                                                    'document_koppelingen_config_png',
                                                'exact' => 1
                                            },
                                            'verzenddatum' => {
                                                '_' => bless(
                                                    {
                                                        '_e'   => [0],
                                                        '_es'  => '+',
                                                        '_m'   => [20141215],
                                                        'sign' => '+'
                                                    },
                                                    'Math::BigFloat'
                                                ),
                                                'exact'               => 1,
                                                'indOnvolledigeDatum' => 'V'
                                            }
                                        }
                                    },
                                    'opmerkingen' => {
                                        '_'     => 'fdfd',
                                        'exact' => 1
                                    },
                                    'soort' => [
                                        {
                                            '_'     => 'Anders',
                                            'exact' => 1
                                        }
                                    ],
                                    'toegevoegdDoor' => {
                                        '_'     => '900060025',
                                        'exact' => 1
                                    },
                                    'valtOnderGeheimhouding' => {
                                        '_'     => 'nee',
                                        'exact' => 1
                                    },
                                    'werkzaamheid' => [
                                        {
                                            'volgnummer' => {
                                                '_'     => 1241633,
                                                'exact' => 1
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                    'parameters' => {
                        'indicatorOvername' => 'V',
                        'mutatiesoort'      => 'T'
                    }
                }
            };

            _is_xml_data_ok($data, $expect, "Aanvulling bedrijf OK");

        },
        "Omgevingsloket aanvulling",
    );
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

