# Getting this old testsuite to work

Working with the old testsuite is deprecated, but there are certain tests we just cannot ignore yet. The
testsuite works by running it as follows. Make sure you run it directly from the root from the repository. E.g.: /vagrant.

Make sure the database is initialized:
```
PERL5LIB=t.old/t/lib:t.old/t/inc prove -vl t.old/t/005-initialize-database.t
```

Example running some tests:

**Running some StUF tests:**
```
PERL5LIB=t.old/t/lib:t.old/t/inc prove -vl t.old/t/lib/TestFor/General/Backend/Sysin/Modules/STUFPRS.pm
```
