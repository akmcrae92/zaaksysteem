import {
    openPage,
    openPageAs
} from './../../../../functions/common/navigate';
import {
    getLockButtonText,
    unlock,
    lock
} from './../../../../functions/intern/caseView/casePhase';
import {
    openPhase
} from './../../../../functions/intern/caseView/caseNav';
import {
    statuses
} from './../../../../functions/common/input/caseAttribute';

describe('when opening case 128 and opening phase 1', () => {

    beforeAll(() => {

        openPageAs('admin', 128);

        openPhase(1);

    });

    it('the attributes should be closed', () => {

        let attributes = $$('.phase-form .vorm-field-item'),
            expected = [];

        attributes.each(() => {
            expected.push(false);
        });

        expect(statuses(attributes)).toEqual(expected);

    });

    it('a button to unlock the phase should be present', () => {

        expect(getLockButtonText()).toEqual('ontgrendelen');

    });

    describe('and when unlocking', () => {
    
        beforeAll(() => {
    
            unlock();
    
        });
    
        it('the attributes should be open', () => {
    
            let attributes = $$('.phase-form .vorm-field-item'),
            expected = [];

            attributes.each(() => {
                expected.push(true);
            });

            expect(statuses(attributes)).toEqual(expected);
    
        });

        it('a button to lock the phase should be present', () => {

            expect(getLockButtonText()).toEqual('vergrendelen');

        });

        describe('and when locking', () => {
        
            beforeAll(() => {
        
                lock();
        
            });
        
            it('the attributes should be closed', () => {

                let attributes = $$('.phase-form .vorm-field-item'),
                expected = [];

            attributes.each(() => {
                expected.push(false);
            });

            expect(statuses(attributes)).toEqual(expected);

            });

            it('a button to unlock the phase should be present', () => {

                expect(getLockButtonText()).toEqual('ontgrendelen');

            });
        
        });

    });

});

describe('when opening case 128, opening phase 1, unlocking and refreshing', () => {

    beforeAll(() => {

        openPageAs('admin', 128);

        openPhase(1);

        unlock();

        openPage(128);

        openPhase(1);

    });

    it('the attributes should be closed', () => {

        let attributes = $$('.phase-form .vorm-field-item'),
            expected = [];

        attributes.each(() => {
            expected.push(false);
        });

        expect(statuses(attributes)).toEqual(expected);

    });

    it('a button to unlock the phase should be present', () => {

        expect(getLockButtonText()).toEqual('ontgrendelen');

    });

});

describe('when opening case 129 and opening phase 1', () => {

    beforeAll(() => {

        openPageAs('admin', 129);

        openPhase(1);

    });

    it('a button to unlock the phase should not be present', () => {

        expect($('.phase-header-status button.phase-unlock-button').isDisplayed()).toBe(false);

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
