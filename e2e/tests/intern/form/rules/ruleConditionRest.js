import {
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    getClosedValue
} from './../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const designationOfConfidentiality = $('[data-name="algemeen_vertrouwelijkheidsaanduiding"]');
const objectionAndAppeal = $('[data-name="algemeen_bezwaar_en_beroep_mogelijk"]');
const publication = $('[data-name="algemeen_publicatie"]');
// ZS-16338
// const bag = $('[data-name="algemeen_bag"]');
const lexSilencioPositivo = $('[data-name="algemeen_lex_silencio_positivo"]');
const suspension = $('[data-name="algemeen_opschorten_mogelijk"]');
const extension = $('[data-name="algemeen_verlengen_mogelijk"]');
const penalty = $('[data-name="algemeen_wet_dwangsom"]');
const wkpb = $('[data-name="algemeen_wkpb"]');

describe('when opening a registration form with all basic casetype settings off', () => {

    beforeAll(() => {

        openPageAs();

        const data = {
            casetype: 'Systeemvoorwaarde Algemene instellingen Aan',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(getClosedValue(designationOfConfidentiality)).toEqual('Geheim');
        expect(getClosedValue(objectionAndAppeal)).toEqual('True');
        expect(getClosedValue(publication)).toEqual('True');

        // ZS-16338
        // expect(getClosedValue(bag)).toEqual('True');
        expect(getClosedValue(lexSilencioPositivo)).toEqual('True');
        expect(getClosedValue(suspension)).toEqual('True');
        expect(getClosedValue(extension)).toEqual('True');
        expect(getClosedValue(penalty)).toEqual('True');
        expect(getClosedValue(wkpb)).toEqual('True');

    });

});

describe('when opening the case with all basic casetype settings on', () => {

    beforeAll(() => {

        openPageAs();

        const data = {
            casetype: 'Systeemvoorwaarde Algemene instellingen Uit',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(getClosedValue(designationOfConfidentiality)).toEqual('Uit');
        expect(getClosedValue(objectionAndAppeal)).toEqual('False');
        expect(getClosedValue(publication)).toEqual('False');

        // ZS-16338
        // expect(getClosedValue(bag)).toEqual('False');
        expect(getClosedValue(lexSilencioPositivo)).toEqual('False');
        expect(getClosedValue(suspension)).toEqual('False');
        expect(getClosedValue(extension)).toEqual('False');
        expect(getClosedValue(penalty)).toEqual('False');
        expect(getClosedValue(wkpb)).toEqual('False');

    });

});


/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
