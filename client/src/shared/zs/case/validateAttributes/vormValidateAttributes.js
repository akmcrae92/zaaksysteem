// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import iban from 'iban';
import emailRegex from 'email-regex';
import isEmpty from './../../../vorm/util/isEmpty';
import defaultMessages from './../../../vorm/util/vormValidator/messages';
import isValidUrl from './../../../vorm/util/isValidUrl';
import { isValidCurrency } from './../../../vorm/util/isValidCurrency';
import { isValidNumeric } from './../../../vorm/util/isValidNumeric';

export default (
  vormValidator,
  fields,
  values,
  messages = defaultMessages,
  locals,
  validator
) => {
  return vormValidator(
    fields,
    values,
    messages,
    locals,
    (field, value, validation) => {
      let attribute = field.$attribute,
        empty = isEmpty(value),
        v = validation,
        type = attribute ? attribute.type : field.template;

      switch (type) {
        case 'bankaccount':
          if (!empty && !iban.isValid(value)) {
            v.bankaccount = messages.bankaccount || true;
          }
          break;
        case 'email':
          if (!empty && !emailRegex({ exact: true }).test(value)) {
            v.email = messages.email || true;
          }
          break;
        case 'numeric':
          if (!empty && !isValidNumeric(value)) {
            v.number = messages.number || true;
          }
          break;
        case 'valuta':
        case 'valutaex':
        case 'valutaex21':
        case 'valutaex6':
        case 'valutain':
        case 'valutain21':
        case 'valutain6':
          if (!empty && !isValidCurrency(value)) {
            v.valuta = messages.valuta || true;
          }
          break;
        case 'url':
          if (!empty && !isValidUrl(value)) {
            v.url = messages.url || true;
          }
          break;
      }

      if (validator) {
        v = validator(field, value, v);
      }

      return validation;
    }
  );
};
