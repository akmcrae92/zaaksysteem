// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const DEFAULT_LIMIT_OPTIONS = [1, 2, 5, 10, 20, 50];

export default function (scope) {
  let ctrl = this;

  let moveTo = (page) => {
    ctrl.onPageChange({
      $page: page,
    });
  };

  ctrl.next = () => {
    moveTo(ctrl.currentPage() - 1);
  };

  ctrl.prev = () => {
    moveTo(ctrl.currentPage() + 1);
  };

  ctrl.handleLimitChange = () => {
    ctrl.onLimitChange({ $limit: ctrl.displayedLimit });
  };

  ctrl.getLimitOptions = () => DEFAULT_LIMIT_OPTIONS;

  scope.$watch(ctrl.limit, (limit) => {
    ctrl.displayedLimit = limit;
  });
}
