// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default (values) => {
  return values.related_subject_role !== 'anders'
    ? values.related_subject_role
    : values.related_subject_role_freeform;
};
