// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import shortid from 'shortid';

const columns = [
  {
    id: 'type',
    label: 'Type',
  },
  {
    id: 'label',
    label: 'Naam',
  },
];

const relatedObjectsTable = (
  $state,
  relatedObjects,
  hasRelatableObjectTypes,
  auxiliaryRouteService
) => {
  const items = relatedObjects.map((object) => ({
    id: shortid(),
    type: object.object_class_name,
    label: object.name,
    href: `/object/${object.uuid}`,
  }));

  const collectionActions = hasRelatableObjectTypes
    ? [
        {
          type: 'link',
          data: {
            label: 'Relateer object',
            href: $state.href(
              auxiliaryRouteService.append($state.current, 'admin'),
              { action: 'object-relateren' },
              { inherit: true }
            ),
          },
        },
      ]
    : [];

  return {
    name: 'related_objects',
    label: 'Gerelateerde objecten',
    href: true,
    columns,
    items,
    collectionActions,
  };
};

export default relatedObjectsTable;
