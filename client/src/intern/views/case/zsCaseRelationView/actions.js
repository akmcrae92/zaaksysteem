// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import mutationServiceModule from './../../../../shared/api/resource/mutationService';
import snackbarServiceModule from './../../../../shared/ui/zsSnackbar/snackbarService';
import propCheck from './../../../../shared/util/propCheck';
import assign from 'lodash/assign';
import omit from 'lodash/omit';

// v0 to v2
const subjectTypeDict = {
  natuurlijk_persoon: 'person',
  bedrijf: 'organization',
  medewerker: 'employee',
};

export default angular
  .module('caseRelationActions', [
    angularUiRouterModule,
    mutationServiceModule,
    snackbarServiceModule,
  ])
  .factory('caseRelationActions', [
    '$state',
    'snackbarService',
    ($state, snackbarService) => {
      return [
        {
          type: 'case/relation/scheduled_job/add',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                case_uuid: propCheck.string,
                casetype_uuid: propCheck.string,
                job: propCheck.string,
                next_run: propCheck.string,
                interval_period: propCheck.string,
                interval_value: propCheck.number,
                runs_left: propCheck.number,
                copy_relations: propCheck.bool,
              }),
              mutationData
            );

            const data = assign(
              { type: 'scheduled_job', job: 'CreateCase' },
              mutationData
            );
            return {
              url: '/api/v1/scheduled_job/create',
              data,
            };
          },
          reduce: (data, mutationData) =>
            data.concat({
              casetype: mutationData.casetype_uuid,
              interval_period: mutationData.interval_period,
              interval_value: mutationData.interval_value,
              next_run: mutationData.next_run,
              runs_left: mutationData.runs_left,
              uuid: mutationData.case_uuid,
            }),
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Geplande zaak wordt aangemaakt', {
              promise,
              then: () => {
                let actions = [];

                if ($state.current.name.indexOf('case.relations') === -1) {
                  actions = [
                    {
                      type: 'link',
                      label: 'Relaties bekijken',
                      link: $state.href('case.relations', null, {
                        inherit: true,
                      }),
                    },
                  ];
                }

                return {
                  message: 'Geplande zaak is aangemaakt',
                  actions,
                };
              },
              catch: () =>
                'Geplande zaak kon niet worden aangemaakt. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
        {
          type: 'case/relation/scheduled_job/update',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                reference: propCheck.string,
                values: propCheck.shape({
                  next_run: propCheck.string,
                  interval_period: propCheck.string,
                  interval_value: propCheck.number,
                  runs_left: propCheck.number,
                  copy_relations: propCheck.bool,
                }),
              }),
              mutationData
            );

            const data = assign(
              { type: 'scheduled_job', job: 'CreateCase' },
              mutationData.values
            );
            return {
              url: `/api/v1/scheduled_job/${mutationData.reference}/update`,
              data,
            };
          },
          reduce: (data, mutationData) =>
            data.map((plannedCase) => {
              if (plannedCase.uuid === mutationData.reference) {
                return plannedCase.merge(mutationData.values, { deep: true });
              }

              return plannedCase;
            }),
          wait: (mutationData, promise) => {
            return snackbarService.wait('', {
              collapse: 0,
              promise,
              catch: () =>
                'Geplande zaak kon niet worden bewerkt. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
          options: {
            reloadOnComplete: true,
          },
        },
        {
          type: 'case/relation/scheduled_job/remove',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                reference: propCheck.string,
              }),
              mutationData
            );

            return {
              url: `/api/v1/scheduled_job/${mutationData.reference}/delete`,
            };
          },
          reduce: (data, mutationData) =>
            data.filter(
              (plannedCase) => plannedCase.uuid !== mutationData.reference
            ),
          options: {
            reloadOnComplete: true,
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Geplande zaak wordt verwijderd', {
              promise,
              then: () => 'Geplande zaak is verwijderd',
              catch: () =>
                'Geplande zaak kon niet verwijderd worden. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
        },
        {
          type: 'case/relation/subject/add',
          request: (mutationData) => {
            if (mutationData.subjectType === 'medewerker') {
              propCheck.throw(
                propCheck.shape({
                  caseUuid: propCheck.string,
                  subjectType: propCheck.string,
                  subjectId: propCheck.string,
                  magic_string_prefix: propCheck.string,
                  role: propCheck.string,
                  employee_authorisation: propCheck.string,
                }),
                mutationData
              );
              return {
                url: '/api/v2/cm/case/create_subject_relation',
                data: {
                  case_uuid: mutationData.caseUuid,
                  subject: {
                    type: subjectTypeDict[mutationData.subjectType],
                    id: mutationData.subjectId,
                  },
                  magic_string_prefix: mutationData.magic_string_prefix,
                  role: mutationData.role,
                  permission: mutationData.employee_authorisation,
                },
              };
            } else {
              propCheck.throw(
                propCheck.shape({
                  caseUuid: propCheck.string,
                  subjectId: propCheck.string,
                  subjectType: propCheck.string,
                  magic_string_prefix: propCheck.string,
                  role: propCheck.string,
                  notify_subject: propCheck.bool,
                  pip_authorized: propCheck.bool,
                }),
                mutationData
              );
              return {
                url: '/api/v2/cm/case/create_subject_relation',
                data: {
                  case_uuid: mutationData.caseUuid,
                  subject: {
                    type: subjectTypeDict[mutationData.subjectType],
                    id: mutationData.subjectId,
                  },
                  magic_string_prefix: mutationData.magic_string_prefix,
                  role: mutationData.role,
                  authorized: mutationData.pip_authorized,
                  send_confirmation_email: mutationData.notify_subject,
                },
              };
            }
          },
          reduce: (data) => data,
          wait: (mutationData, promise) => {
            let actions = [];

            if ($state.current.name.indexOf('case.relations') === -1) {
              actions = [
                {
                  type: 'link',
                  link: $state.href('case.relations', null, { inherit: true }),
                  label: 'Relaties bekijken',
                },
              ];
            }

            return snackbarService.wait('Betrokkene wordt toegevoegd.', {
              promise,
              then: () => {
                return {
                  message: 'Betrokkene is toegevoegd aan het zaakdossier.',
                  actions,
                };
              },
              catch: () =>
                'Betrokkene kon niet worden toegevoegd aan het zaakdossier. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
          options: {
            reloadOnComplete: true,
          },
        },
        {
          type: 'case/relation/subject/remove',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                relationId: propCheck.string,
              }),
              mutationData
            );

            return {
              url: '/api/v2/cm/case/delete_subject_relation',
              data: {
                relation_uuid: mutationData.relationId,
              },
            };
          },
          reduce: (data, mutationData) => {
            return data.filter(
              (subject) => subject.id !== mutationData.subjectId
            );
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Betrokkene wordt verwijderd', {
              collapse: 0,
              promise,
              catch: () =>
                'Betrokkene kon niet worden verwijderd. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
          options: {
            reloadOnComplete: true,
          },
        },
        {
          type: 'case/relation/subject/update',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                relationId: propCheck.string,
                magic_string_prefix: propCheck.string,
                role: propCheck.string,
                employee_authorisation: propCheck.string.optional,
                permission: propCheck.bool.optional,
              }),
              mutationData
            );

            const specificData =
              mutationData.betrokkene_type === 'medewerker'
                ? { permission: mutationData.employee_authorisation }
                : { authorized: mutationData.pip_authorized };

            return {
              url: '/api/v2/cm/case/update_subject_relation',
              data: assign(
                {
                  relation_uuid: mutationData.relationId,
                  magic_string_prefix: mutationData.magic_string_prefix,
                  role: mutationData.role,
                },
                specificData
              ),
            };
          },
          reduce: (data, mutationData) => {
            return data.map((subject) => {
              if (subject.id === mutationData.subjectId) {
                return subject.merge(omit(mutationData, 'subjectId', 'caseId'));
              }

              return subject;
            });
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Betrokkene wordt bewerkt', {
              collapse: 0,
              promise,
              catch: () =>
                'Betrokkene kon niet worden bewerkt. Neem contact op met uw beheerder voor meer informatie.',
            });
          },
          options: {
            reloadOnComplete: true,
          },
        },
        {
          type: 'case/relation/reschedule',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                caseId: propCheck.number,
              }),
              mutationData
            );

            return {
              url: `/zaak/${mutationData.caseId}/reschedule/?action=update`,
            };
          },
          reduce: (data /*, mutationData*/) => data,
          wait: (mutationData, promise) => {
            return snackbarService.wait('E-mails worden opnieuw ingepland', {
              promise,
              collapse: 0,
              then: () => 'E-mails zijn opnieuw ingepland',
              catch: () =>
                'E-mails konden niet opnieuw worden ingepland. Neem contact op met uw beheerder voor meer informatie',
            });
          },
          options: {
            reloadOnComplete: true,
          },
        },
      ];
    },
  ])
  .run([
    'mutationService',
    'caseRelationActions',
    (mutationService, actions) => {
      actions.forEach((action) => {
        mutationService.register(action);
      });
    },
  ]).name;
