// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default (docdataSlices) => {
  let docdata = [],
    fileContent = new String(),
    bytes,
    file;

  for (let i = 0; i < docdataSlices.length; i++) {
    docdata = docdata.concat(docdataSlices[i]);
  }

  for (let j = 0; j < docdata.length; j++) {
    fileContent += String.fromCharCode(docdata[j]);
  }

  bytes = new Uint8Array(fileContent.length);

  for (let i = 0; i < bytes.length; i++) {
    bytes[i] = fileContent.charCodeAt(i);
  }

  // We create a Blob instead of File as Safari and IE don't support File, and the Word Add In uses
  // either engine to render within the context of Word.
  file = new Blob([bytes], {
    type:
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  });
  file.lastModifiedDate = new Date();

  return file;
};
