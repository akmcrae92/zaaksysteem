BEGIN;

-- Implementation of 'all users' via 'behandelaar' role is deprecated, insert
-- new tag-based security entities for all (case)types

INSERT INTO object_acl_entry (object_uuid, entity_type, entity_id, capability, scope)
    SELECT uuid, 'tag', 'all_users', 'read', 'instance' FROM object_data WHERE object_class IN ('casetype', 'type');

COMMIT;
