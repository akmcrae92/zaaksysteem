
BEGIN;

  DROP INDEX IF EXISTS object_data_case_date_current_idx;

  CREATE INDEX object_data_case_date_current_idx
    ON object_data(CAST(hstore_to_timestamp(index_hstore->'case.date_current'::text) AS DATE));

COMMIT;
