BEGIN;

  DROP TABLE IF EXISTS subject_login_history;

  CREATE TABLE subject_login_history (
    id SERIAL primary key,
    ip inet NOT NULL,
    subject_id int REFERENCES subject(id) NOT NULL,
    subject_uuid uuid REFERENCES subject(uuid) NOT NULL,
    success boolean default true NOT NULL,
    method text NOT NULL,
    date_attempt TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL
  );

COMMIT;
