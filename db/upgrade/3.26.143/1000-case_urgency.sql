BEGIN;

    ALTER TABLE zaak ADD COLUMN urgency TEXT CHECK (urgency IN ('normal', 'medium', 'high', 'late'));

COMMIT;
