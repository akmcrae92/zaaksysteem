BEGIN;

ALTER TABLE config ADD constraint parameter_unique UNIQUE(parameter);

COMMIT;
