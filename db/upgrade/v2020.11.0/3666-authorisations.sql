BEGIN;

  CREATE OR REPLACE FUNCTION migrate_ztb_fase_auth() RETURNS void LANGUAGE plpgsql AS $$
  DECLARE
    r record;
    counter int;
    nested int[];
  BEGIN
        FOR r IN
          SELECT DISTINCT
            zt.zaaktype_node_id,
            zt.id,
            zts.ou_id,
            zts.role_id
          FROM zaaktype zt
          JOIN
            zaaktype_status zts
          ON zt.zaaktype_node_id = zts.zaaktype_node_id
          ORDER BY zt.zaaktype_node_id asc
        LOOP

          SELECT INTO counter count(id) from roles where id = r.role_id;
          IF counter = 0 THEN
            RAISE NOTICE 'Configured role % does not exist!', r.role_id;
            CONTINUE;
          END IF;

          SELECT INTO nested path from groups where id = r.ou_id;
          IF nested IS NULL THEN
            RAISE NOTICE 'Configured group % does not exist!', r.ou_id;
            CONTINUE;
          END IF;

          SELECT INTO counter count(*) FROM zaaktype_authorisation WHERE
            zaaktype_node_id = r.zaaktype_node_id
          AND
            role_id = r.role_id
          AND
            array[ou_id] <@ nested
          ;

          -- Have entries, no need to worry
          IF counter > 0 THEN
            CONTINUE;
          END IF;

          -- insert missing permissions for assignment group/role from
          -- status
          INSERT INTO zaaktype_authorisation
            (zaaktype_node_id, zaaktype_id, created, last_modified, ou_id,
             role_id, recht)
          VALUES
            (r.zaaktype_node_id, r.id, NOW(), NOW(), r.ou_id, r.role_id, 'zaak_search'),
            (r.zaaktype_node_id, r.id, NOW(), NOW(), r.ou_id, r.role_id, 'zaak_read'),
            (r.zaaktype_node_id, r.id, NOW(), NOW(), r.ou_id, r.role_id, 'zaak_edit');

        END LOOP;
      RETURN;

  END $$;

  SELECT migrate_ztb_fase_auth();

  DROP FUNCTION migrate_ztb_fase_auth;

COMMIT;
