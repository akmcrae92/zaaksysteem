/*
    This script will update `zaak`.
    
    Rows that have a 'resultaat', but no 'resultaat_id' will be update to use
    the new identifier for results.
    
    It will not update when there are multiple 'resultaten' by the same name and
    multiple 'standaard_keuze' are found. That is a deliberate misconfigurartion
    Those that have no preference, we will use the first.
*/


BEGIN;

WITH

-- count those that
-- - have the same resultaat
-- - for the same zaaktype_node_id,
-- - and are set as standaard_keuze

zaaktype_resultaten_counted AS (
    SELECT
        *,
        COUNT(*) OVER(
            PARTITION BY
                resultaat,
                zaaktype_node_id
            ) AS COUNTER
    FROM
        zaaktype_resultaten
    WHERE
        standaard_keuze = TRUE

),

-- get the ones that are counted for as more than once
-- those are the bad configured

zaaktype_resultaten_multiple AS (
    SELECT
        *
    FROM
        zaaktype_resultaten_counted
    WHERE
        COUNTER > 1
),

-- within a partition of zaaktype_node_id/resultaat
--
-- get the prefered one, that is a standaard_keuze, otherwise the first
--
-- but skip the badly configured

zaaktype_resultaten_options AS (
    SELECT
        id,
        zaaktype_node_id,
        resultaat,
        label,
        standaard_keuze,
        RANK() OVER(
            PARTITION BY
                zaaktype_node_id,
                resultaat
            ORDER BY
                standaard_keuze DESC,
                id ASC
            )
    FROM
        zaaktype_resultaten
    WHERE
        id NOT IN (
        SELECT id FROM zaaktype_resultaten_multiple
        )
),

zaaktype_resultaten_prefered AS (
    SELECT
        *
    FROM
        zaaktype_resultaten_options
    WHERE
        rank = 1
)

UPDATE
    zaak
SET (
    resultaat_id
) = (
    SELECT
        prefered.id
    FROM
        zaaktype_resultaten_prefered as prefered
    WHERE
        zaak.zaaktype_node_id = prefered.zaaktype_node_id
        AND
        zaak.resultaat = prefered.resultaat
)
WHERE
    resultaat IS NOT NULL
    AND
    resultaat_id IS NULL
;


COMMIT;
