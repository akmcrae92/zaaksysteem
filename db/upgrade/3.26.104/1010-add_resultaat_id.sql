BEGIN;
    /* ZS-16932_add_resultaat_id.sql */

    ALTER TABLE zaak
        ADD COLUMN IF NOT EXISTS resultaat_id
            INTEGER
            REFERENCES zaaktype_resultaten (id)
    ;

COMMIT;
