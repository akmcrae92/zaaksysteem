BEGIN;

    -- Remove old conflicing named constraint
    ALTER TABLE zaak_kenmerk DROP CONSTRAINT IF EXISTS zaak_kenmerk_zaak_id_idx;

    -- Ensure zaak kenmerken don't prevent case deletions
    ALTER TABLE zaak_kenmerk DROP CONSTRAINT IF EXISTS zaak_kenmerk_zaak_id_fkey;
    ALTER TABLE zaak_kenmerk ADD CONSTRAINT zaak_kenmerk_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES zaak(id) ON DELETE CASCADE;

    -- Ensure a unique INDEX (not constraint) exists for the kenmerken table
    DROP INDEX IF EXISTS zaak_kenmerk_bibliotheek_kenmerken_id;
    CREATE UNIQUE INDEX zaak_kenmerk_bibliotheek_kenmerken_id ON zaak_kenmerk USING btree (zaak_id, bibliotheek_kenmerken_id);

COMMIT;
