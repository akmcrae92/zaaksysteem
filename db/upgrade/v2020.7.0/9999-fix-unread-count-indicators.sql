BEGIN;

-- Fix thread unread counts
WITH unread_counts AS (
    SELECT
        t.id AS thread_id,
        sum(
            CASE WHEN tm.id IS NULL THEN
                0
            ELSE
                1
            END) AS num_unread
    FROM
        thread t
    LEFT JOIN thread_message tm
    JOIN thread_message_external tme ON (tme.id = tm.thread_message_external_id
            AND tme.read_employee IS NULL) ON (t.id = tm.thread_id)
GROUP BY
    t.id)
UPDATE
    thread
SET
    unread_employee_count = unread_counts.num_unread
FROM
    unread_counts
WHERE
    unread_counts.thread_id = thread.id;

-- Fix object_data (search results)
WITH unread_counts AS (
    SELECT
        SUM(COALESCE(t.unread_employee_count, 0)) AS unread,
        z.id AS case_id
    FROM
        zaak z
    LEFT JOIN thread t ON (z.id = t.case_id)
    WHERE z.status != 'deleted'
GROUP BY
    z.id)
UPDATE
    object_data
SET
    properties = jsonb_set(cast(properties AS jsonb), '{values,case.num_unread_communication}', ('{"name": "case.num_unread_communication", "value": ' || u.unread::varchar || ', "human_label": "Ongelezen berichten", "human_value": "' || u.unread::varchar || '", "attribute_type": "integer"}')::jsonb),
    index_hstore = index_hstore || hstore ('case.num_unread_communication',
        u.unread::varchar)
FROM
    unread_counts u
WHERE
    u.case_id = object_data.object_id
    AND object_data.object_class = 'case';

-- Fix case_property (case view)
-- Add a new case property, 'num_unread_communication'/unread_communication_count
INSERT INTO case_property (name, namespace, TYPE, value_v0, case_id, object_id) (
  SELECT
    'unread_communication_count',
    'case',
    'null',
    ('[{"name": "case.num_unread_communication", "value": ' || coalesce(sum(t.unread_employee_count), 0) || ', "human_label": "Ongelezen berichten", "human_value": "' || coalesce(sum(t.unread_employee_count), 0) || '", "attribute_type": "integer"}]')::JSONB,
    z.id,
    z.uuid
  FROM
    zaak z
  LEFT JOIN thread t ON t.case_id = z.id
  WHERE z.uuid IS NOT NULL AND z.status != 'deleted'
GROUP BY
  z.id,
  z.uuid)
ON CONFLICT (name, namespace, object_id, case_id)
  DO UPDATE SET value_v0 = excluded.value_v0;


COMMIT;

