BEGIN;

    ALTER TABLE zaak_subcase DROP COLUMN parent_advance_results;
    ALTER TABLE zaaktype_relatie DROP COLUMN parent_advance_results;

COMMIT;
