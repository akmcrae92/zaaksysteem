BEGIN;

    ALTER TABLE zaaktype_relatie ADD creation_style VARCHAR NOT NULL DEFAULT 'background';
    ALTER TABLE zaaktype_relatie ADD CHECK(creation_style IN ('background', 'form'));

COMMIT;
