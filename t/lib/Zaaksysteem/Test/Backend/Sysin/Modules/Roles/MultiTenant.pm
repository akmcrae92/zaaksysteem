package Zaaksysteem::Test::Backend::Sysin::Modules::Roles::MultiTenant;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Test::Objects;

sub get_sysin_module {
    my $schema = shift // get_schema(
        customer_instance => {
            VirtualHosts => {
                'foo.zs.nl' => { customer_id => 'foo' },
                'bar.zs.nl' => { customer_id => 'bar' }
            },
            instance_hostname => 'mocked.zs.nl',
        }
    );

    my $sysin_module = Zaaksysteem::Test::Sysin::Module->new(schema => $schema);
    ok(
        $sysin_module->does(
            "Zaaksysteem::Backend::Sysin::Modules::Roles::MultiTenant"),
        "Does the MultiTenant role"
    );
    return $sysin_module;
}

sub test_schema_less {

    my $mod = Zaaksysteem::Test::Sysin::Module->new();
    throws_ok(
        sub {
            $mod->schema;
        },
        qr#multitenant/schema/missing#,
        "Schema missing, error is thrown"
    );

    my $schema = get_schema;
    my $copy;
    lives_ok(
        sub {
            $mod->set_schema($schema);
            $copy = $mod->schema;
        },
        "Schema set, no error is thrown"
    );

    cmp_deeply($copy, $schema, "The schema has returned");

}

sub test_multi_tenant_role {

    my $sysin_module = get_sysin_module();

    my $config = $sysin_module->interface_config;
    my $hosts = pop(@$config);

    isa_ok($hosts, "Zaaksysteem::ZAPI::Form::Field");
    cmp_deeply(
        $hosts->data->{options},
        [
            { value => 'bar.zs.nl',    label => 'bar.zs.nl' },
            { value => 'foo.zs.nl',    label => 'foo.zs.nl' },
            { value => 'mocked.zs.nl', label => 'mocked.zs.nl' },
        ],
        "Hosts fields are defined correctly"
    );

    is($hosts->when, 'interface_is_multi_tenant === true', "JS when is supplied");

}

sub test_multi_tenant_is_no_tenant {

    my $schema = get_schema(customer_instance => { VirtualHosts => {} });
    my $sysin_module = get_sysin_module($schema);
    my $config = $sysin_module->interface_config;
    is(@$config, 0, "No interface_config for multi tenant");

    {
        note ("Empty virtualhost");
        my $schema = get_schema(customer_instance => { VirtualHosts => {} });
        my $sysin_module = get_sysin_module($schema);
        my $config = $sysin_module->interface_config;
        is(@$config, 0, "No interface_config for multi tenant");
    }

    {
        note ("Empty schema (unlikely to happen)");
        my $schema = get_schema();
        my $sysin_module = get_sysin_module($schema);
        my $config = $sysin_module->interface_config;
        is(@$config, 0, "No interface_config for multi tenant");
    }
}


package Zaaksysteem::Test::Sysin::Module {
    use Moose;
    with qw(Zaaksysteem::Backend::Sysin::Modules::Roles::MultiTenant);

    sub build_config_fields {
        my $self = shift;
        return [];
    };

    sub log {
        my $self = shift;
        return $self->_log;
    }

    has _log => (
        is      => 'ro',
        default => sub { Log::Log4perl->get_logger('Zaaksysteem') },
        lazy    => 1,
    );

};


__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Modules::Roles::MultiTenant - Test the multi tenant role for Sysin::Modules infra

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Backend::Sysin::Modules::Roles::MultiTenant

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
