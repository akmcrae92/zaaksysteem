package Zaaksysteem::Test::Object::Types::LegalEntityType;

use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;

use Zaaksysteem::Object::Types::LegalEntityType;

sub test_new {
    my $legal_entity_type;
    lives_ok(
        sub {
            $legal_entity_type = Zaaksysteem::Object::Types::LegalEntityType
                ->new(
                    code        => 1,
                    label       => 'Foo',
                    description => 'Test Foo type',
                    active      => 1,
                )
        },
        'Creates a new object'
    );
    isa_ok($legal_entity_type, 'Zaaksysteem::Object::Types::LegalEntityType');
}

sub test_new_from_code {
    my $legal_entity_type;
    lives_ok(
        sub {
            $legal_entity_type = Zaaksysteem::Object::Types::LegalEntityType
                ->new_from_code( 73 )
        },
        'Creates a new object from code'
    );
    is($legal_entity_type->label, 'Kerkgenootschap', '... and is an "Kerkgenootschap"');
}

sub test_new_from_label {
    my $legal_entity_type;
    lives_ok(
        sub {
            $legal_entity_type = Zaaksysteem::Object::Types::LegalEntityType
                ->new_from_label( 'Privaatrechtelijke rechtspersoon' )
        },
        'Creates a new object from label'
    );
    is($legal_entity_type->code, '89', '... and has code "89"');

    undef $legal_entity_type;
    throws_ok(
        sub {
            $legal_entity_type = Zaaksysteem::Object::Types::LegalEntityType
                ->new_from_label( 'Cooperatie' ) # Coöperatie
        }, qr|object/types/legalentitytype/unknown_label|,
        'Does need to match exactly'
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Object::Types::LegalEntityType; - Test LegalEntityType

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Object::Types::LegalEntityType;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
