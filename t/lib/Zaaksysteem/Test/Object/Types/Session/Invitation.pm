package Zaaksysteem::Test::Object::Types::Session::Invitation;

use Zaaksysteem::Test;

use JSON::XS ();
use MIME::Base64 qw[decode_base64];
use URI;

use Zaaksysteem::Object::Types::Session::Invitation;

sub test_session_invitation_uri {
    my $subject = mock_one(
        'X-Mock-ISA' => [qw[
            Zaaksysteem::Object::Types::Subject
            Zaaksysteem::Object::Reference::Instance
            Moose::Object
        ]],
        type => 'subject'
    );

    my $invitation = Zaaksysteem::Object::Types::Session::Invitation->new(
        subject => $subject,
        date_expires => DateTime->now,
        token => 'abc'
    );

    # v2 URIs
    {
        my $uri = $invitation->as_uri(URI->new('https://example.com/bar/baz'), 3);

        ok $uri =~ m#^zaaksysteem:\/\/(?<blob>.*)$#, 'zaaksysteem protocol uri';

        ok defined $+{blob}, 'zaaksysteem uri blob';

        my $json_str = decode_base64($+{blob});

        ok length $json_str, 'base64 decode returns content';

        my $data;

        lives_ok {
            $data = JSON::XS->new->decode($json_str);
        } 'json decode ok';

        isa_ok($data, 'HASH', 'blob data json string');

        cmp_deeply(
            $data,
            {
                'auth_token' => 'abc',
                'base_uri' => 'example.com/bar/baz',
                'download' => 'download?case_id=3',
                'lock_acquire' => 'lock/acquire?case_id=3',
                'lock_extend' => 'lock/extend?case_id=3',
                'lock_get' => 'lock?case_id=3',
                'lock_release' => 'lock/release?case_id=3',
                'upload' => 'upload?case_id=3',
                'version' => 2
            },
            "Returned data structure is correct"
        );
    }

    # Old-style v1 URIs; to be removed in 2019
    {
        my $uri = $invitation->as_v1_uri(URI->new('https://example.com/bar/baz'));

        ok $uri =~ m#^zaaksysteem:\/\/(?<blob>.*)$#, 'zaaksysteem protocol uri';
        ok defined $+{blob}, 'zaaksysteem uri blob';

        my $json_str = decode_base64($+{blob});

        ok length $json_str, 'base64 decode returns content';

        my $data;

        lives_ok {
            $data = JSON::XS->new->decode($json_str);
        } 'json decode ok';

        isa_ok($data, 'HASH', 'blob data json string');

        is $data->{ auth_token }, 'abc', 'encoded auth token';
        is $data->{ base_uri }, 'example.com', 'encoded base_uri';

        ok(exists $data->{ $_ }, "$_: expected action exists") for qw[
            download
            upload
            lock_get
            lock_acquire
            lock_extend
            lock_release
        ];

        like($data->{$_}, qr{^/bar/baz}, "action URLs '$_' contains the full path") for qw[
            download
            upload
            lock_acquire
            lock_extend
            lock_get
            lock_release
        ];
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
