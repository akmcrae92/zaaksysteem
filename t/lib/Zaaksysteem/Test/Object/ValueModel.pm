package Zaaksysteem::Test::Object::ValueModel;

use Zaaksysteem::Test;

use Zaaksysteem::Object::ValueModel;

use Scalar::Util qw[refaddr];

sub test_object_value_model {
    ok(Zaaksysteem::Object::ValueModel->preload, 'preload returns true-ish');

    my $model = Zaaksysteem::Object::ValueModel->get_instance;

    isa_ok $model, 'Zaaksysteem::Object::ValueModel', 'singleton getter';

    is refaddr($model), refaddr(Zaaksysteem::Object::ValueModel->get_instance),
        'get_instance returns singleton instance every time';

    ok keys %{ $model->value_types }, 'value_types initialized';

    # Checking test-context requirements
    ok exists $model->value_types->{ string }, 'string type exists';

    my $value = Zaaksysteem::Object::ValueModel->new_value(string => 'abc');

    isa_ok $value, 'Zaaksysteem::Object::Value', 'value instance returned';
    is $value->type_name, 'string', 'value type correctly set';

    dies_ok {
        Zaaksysteem::Object::ValueModel->new_value(__test__ => 'foo');
    } 'non-existant type dies';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
