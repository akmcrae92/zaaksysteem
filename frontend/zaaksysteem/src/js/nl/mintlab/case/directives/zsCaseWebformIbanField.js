// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseWebformIbanField', [
    function () {
      return {
        require: ['zsCaseWebformIbanField', '^zsCaseWebformField'],
        controller: [
          function () {
            var ctrl = this,
              zsCaseWebformField,
              zsCaseIbanValidate;

            function setSetter() {
              if (zsCaseWebformField && zsCaseIbanValidate) {
                zsCaseWebformField.setSetter(function (value) {
                  zsCaseIbanValidate.setValue(value);
                });
              }
            }

            ctrl.setIbanValidate = function (ibanValidate) {
              zsCaseIbanValidate = ibanValidate;
              setSetter();
            };

            ctrl.unsetIbanValidate = function () {
              zsCaseIbanValidate = null;
              zsCaseWebformField.setSetter(undefined);
            };

            ctrl.link = function (controllers) {
              zsCaseWebformField = controllers[0];
              setSetter();
            };

            return ctrl;
          },
        ],
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();
