// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.shims.trim', function () {
    if (String.prototype.trim !== undefined) {
      return function (string) {
        return string && typeof string === 'string' ? string.trim() : undefined;
      };
    } else {
      return function (string) {
        return string && typeof string === 'string'
          ? string.replace(/^\s+|\s+$/g, '')
          : undefined;
      };
    }
  });
})();
