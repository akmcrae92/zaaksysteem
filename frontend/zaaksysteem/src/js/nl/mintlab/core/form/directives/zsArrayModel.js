// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsArrayModel', [
    function () {
      return {
        require: ['zsArrayModel', 'ngModel'],
        priority: -1,
        controller: [
          function () {
            var ctrl = this,
              ngModel;

            function getModelValue() {
              return ngModel.$modelValue || [];
            }

            ctrl.setModel = function (model) {
              ngModel = model;

              ngModel.$isEmpty = function () {
                return _.isEmpty(ngModel.$modelValue);
              };
            };

            ctrl.getList = function () {
              return getModelValue();
            };

            ctrl.addObject = function (object) {
              var array = getModelValue();

              array.push(object);

              ngModel.$setViewValue(array.concat());
            };

            ctrl.removeObject = function (object) {
              var array = getModelValue();

              _.pull(array, object);

              ngModel.$setViewValue(array.concat());
            };

            ctrl.hasObject = function (obj) {
              return _.indexOf(getModelValue(), obj) !== -1;
            };

            return ctrl;
          },
        ],
        controllerAs: 'arrayModel',
        link: function (scope, element, attrs, controllers) {
          var zsArrayModel = controllers[0],
            ngModel = controllers[1];

          zsArrayModel.setModel(ngModel);
        },
      };
    },
  ]);
})();
