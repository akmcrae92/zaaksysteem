// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsCrudTableAutoSizeCell', [
    function () {
      return {
        require: '^zsCrudTableAutoSizeRow',
        link: function (scope, element, attrs, zsCrudTableAutoSizeRow) {
          zsCrudTableAutoSizeRow.register(element[0]);

          scope.$on('destroy', function () {
            zsCrudTableAutoSizeRow.unregister(element[0]);
          });
        },
      };
    },
  ]);
})();
