package Zaaksysteem::Helper::ToJSON;
use Moose;
use MooseX::NonMoose;

extends 'DBIx::Class::Helper::Row::ToJSON';

=head1 NAME

Zaaksysteem::Helper::ToJSON - Subclass of DBIx::Class::Helper::Row::ToJSON that allows serialization of "text" type columns.

=head1 SYNOPSIS

    # DBIx::Class initialization here
    #
    __PACKAGE__->load_components('+Zaaksysteem::Helper::ToJSON');

=head1 DESCRIPTION

Overrides the "_is_column_serializable" helper in
L<DBIx::Class::Helper::Row::ToJSON> to always return a true value, so all
columns are always serialized.

For some reason, the base class doesn't allow serialization of columns of type
"text" by default.

=head1 METHODS

=head2 _is_column_serializable

Always returns true - we want to serialize ALL the columns.

=cut

override '_is_column_serializable' => sub {
    return 1;
};

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
