package Zaaksysteem::Controller::API::Config;
use Moose;
use namespace::autoclean;

use BTTW::Tools;
use JSON;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head2 use_flexpaper

Make use of flexpaper or not (you will use WebODF).

=cut

sub use_flexpaper : Local : ZAPI {
    my ($self, $c) = @_;
    my $bool = $c->model('DB::Config')->get('use_flexpaper') || 0;
    $c->stash->{zapi} = [ { use_flexpaper => $bool ? JSON::true : JSON::false }];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
