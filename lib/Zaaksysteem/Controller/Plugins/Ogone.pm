package Zaaksysteem::Controller::Plugins::Ogone;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Constants qw(
    CASE_PAYMENT_STATUS_FAILED
    CASE_PAYMENT_STATUS_OFFLINE
    CASE_PAYMENT_STATUS_PENDING
    CASE_PAYMENT_STATUS_SUCCESS
);

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('plugins/ogone/api'): CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{nowrapper} = 1;

    $c->stash->{template}       = 'plugins/ogone/mislukt.tt';
    $c->stash->{ogone_error}    = 'Uw betaling is mislukt.';

    ### Loop over given parameters
    $c->stash->{ogone}      = $c->model('Plugins::Ogone');

    $c->stash->{ogone}->verify_payment(
        %{ $c->req->params },
    );

    $c->detach unless $c->stash->{ogone}->verified;

    ### Extra check. Make sure the amount is correct
    my $orderid     = $c->stash->{ogone}->orderid;

    my ($zaaknr)    = $orderid =~ /z(\d+)/;

    $c->stash->{zaak} = $c->model('DB::Zaak')->find($zaaknr);

    return if $c->stash->{zaak};

    my $queue_item = $self->_get_queue_item($c, $zaaknr);

    if ($queue_item) {
        $c->stash->{postponed} = $queue_item;
        return;
    }

    $c->log->info("Unable to find case with id $zaaknr for Ogone");
    return;
}

sub _get_queue_item {
    my ($self, $c, $case_id) = @_;

    my $rs = $c->model('DB::Queue')->search_rs;
    my $jsonb = $c->model('DB')->schema->encode_jsonb(
        {
            case_id     => $case_id,
            create_args => { payment_status => 'pending' },
        }
    );

    my $queue_item = $rs->search_rs(
        {
            type          => 'create_case_form',
            status        => 'postponed',
            "data::jsonb" => { '@>' => \$jsonb },
        }
    )->first;

    return $queue_item if $queue_item;
    return;
}

sub payment : Chained('/') : PathPart('plugins/ogone/payment') : Args(0) {
    my ($self, $c) = @_;

    throw(
        'payment/delayed/no_case',
        "Payment code invoked without a case",
    ) unless $c->session->{payment_case_id};

    $c->stash->{payment_case_id} = $c->session->{payment_case_id};

    # Possible reload of case, dupe, whatevs
    if (!$c->stash->{postponed}) {
        $c->stash->{postponed} = $self->_get_queue_item(
            $c,
            $c->session->{payment_case_id}
        );
        throw('payment/delayed/no_queue',
            "Payment code invoked without a propper case",
        ) if !$c->stash->{postponed};
    }

    if (!$c->stash->{zaaktype}) {
        my $casetype = $self->_get_casetype_for_delayed_case($c);
        $c->stash->{zaaktype} = $casetype->zaaktype_node_id;
    }


    my $human_amount = $c->stash->{postponed}->data->{create_args}{payment_amount};

    my $amount_in_cents = $self->amount_in_cents($human_amount);

    $c->stash->{ogone} = $c->model('Plugins::Ogone');
    $c->stash->{ogone_amount} = $human_amount;

    $c->stash->{ogone}->start_payment(
        amount     => $amount_in_cents,
        zaak_id    => $c->session->{payment_case_id},
        context    => $c->session->{delayed_context},
    );

    $c->stash->{nowrapper} = undef;
    $c->stash->{template}  = 'plugins/ogone/betaling_delayed.tt';
}


sub betaling : Chained('/') : PathPart('plugins/ogone/betaling') : Args(0) {
    my ($self, $c) = @_;

    throw(
        'payment/no_case',
        "Payment code invoked without a case",
    ) unless $c->session->{payment_case_id};

    $c->stash->{zaak} = $c->model('DB::Zaak')->find($c->session->{payment_case_id});
    $c->stash->{zaaktype} = $c->stash->{zaak}->zaaktype_node_id;

    if ($c->stash->{zaak}->payment_status) {
        throw(
            'payment/ogone/payment_already_initiated',
            'A payment has already been started for this case'
        );
    }

    my $human_amount = $c->stash->{zaak}->payment_amount
        // $c->stash->{zaak}->zaaktype_node_id->zaaktype_definitie_id->pdc_tarief;

    my $amount_in_cents = $self->amount_in_cents($human_amount);

    $c->stash->{ogone} = $c->model('Plugins::Ogone');
    $c->stash->{ogone_amount} = $human_amount;

    $c->stash->{ogone}->start_payment(
        amount => $amount_in_cents,
        zaak   => $c->stash->{ zaak },
    );

    $c->stash->{zaak}->payment_amount($human_amount); # not so human after all
    $c->stash->{zaak}->set_payment_status(CASE_PAYMENT_STATUS_PENDING, $human_amount);
    $c->stash->{zaak}->update();

    $c->stash->{nowrapper} = undef;
    $c->stash->{template}  = 'plugins/ogone/betaling.tt';
}


sig amount_in_cents => 'Num';

sub amount_in_cents {
    my ($self, $amount) = @_;

    # numbers + optionally a dot and 2 numbers
    # 123232 => OK
    # 12323. => wrong
    # 223223.1 => OK
    # 3231.34 => OK
    # .232332 => wrong
    # 0.22332 => OK

    $self->log->debug(sprintf("Original amount: %s", $amount));

    if ($amount && $amount =~ m|^\d+(\.\d+)?$|) {
        my $cents = $amount * 100;
        $self->log->debug("Ogone amount $amount in cents: $cents");
        return $cents;
    }
    else {
        throw("ogone/amount/invalid", "Invalid amount given : $amount");
    }
}



=head2 accept_delayed

Be able to accept delayed cases which needs some changes in their data
structure to be created with ogone payment handling

=cut

sub accept_delayed : Private {
    my ($self, $c) = @_;

    my $ogone_amount  = $c->stash->{ogone}->amount;
    my $stored_amount = $self->amount_in_cents(
        $c->stash->{postponed}->data->{create_args}{payment_amount}
    );

    throw('plugin/ogone/corrupt_state',
        'Ogone payment amount and locally stored amount do not match, aborting. ' .
        "Ogone: $ogone_amount, stored: $stored_amount",
    ) unless $ogone_amount eq $stored_amount;

    $c->log->debug(sprintf(
        'Ogone transaction validated and accepted, payment received: %d¢',
        $stored_amount
    ));


    $self->update_payment_status_and_queue($c, CASE_PAYMENT_STATUS_SUCCESS);
    return 1;
}

sub _get_sql_for_payment_status {
    my $status = shift;

    $status = sprintf("to_jsonb('%s'::text)", $status);
    return "jsonb_set(data::jsonb, '{create_args,payment_status}', $status)";
}

sub _get_casetype_for_delayed_case {
    my ($self, $c) = @_;

    my $cid = $c->stash->{postponed}->data->{create_args}{zaaktype_id};
    my $casetype = $c->model("DB::Zaaktype")
        ->find($cid, { prefetch => 'zaaktype_node_id' });
    return $casetype if $casetype;
    throw(
        'plugin/ogone/casetype/not_found',
        "Unable to find casetype with id $cid"
    );
}

sub accept : Chained('base') : PathPart('accept'): Args(0) {
    my ($self, $c) = @_;

    unless ($c->stash->{ogone}->succes) {
        throw(
            'plugin/ogone/unsuccesful',
            'Ogone transaction indicates unsuccesful transaction. Bailing out.'
        );
    }

    if ($c->stash->{postponed}) {
        $c->forward('accept_delayed');

        my $casetype = $self->_get_casetype_for_delayed_case($c);
        $c->stash->{zaaktype} = $casetype->zaaktype_node_id;
        $c->stash->{delayed_response} = delete $c->stash->{postponed};
        $c->stash->{nowrapper} = 1;
        $c->stash->{betaling}  = 1;
        $c->forward('/zaak/finish_delayed');
        $c->detach();
        return;
    }

    # Some clarification is in order here, why bail out at all?
    # Payment transations are started once, and can only be finished once. So
    # checking the that the current status for this case is 'pending' is a
    # safe-guard against clickspam and other state-corrupting flows to this sub.
    if ($c->stash->{ zaak }->payment_status eq CASE_PAYMENT_STATUS_PENDING) {
        $c->forward('assert_amount');

        my $human_amount = sprintf("%.2f", ($c->stash->{ogone}->amount / 100));

        $c->stash->{zaak}->set_payment_status(CASE_PAYMENT_STATUS_SUCCESS, $human_amount);

        # Skipped notifications, now is time to send
        $c->forward('/zaak/_create_zaak_fire_phase_actions', [ 'email' ]);
    } else {
        $c->log->warn(sprintf(
            'Skipping acceptance of payment for case %d which is not in-transation (pending confirmation).',
            $c->stash->{ zaak }->id
        ));
    }

    if ($c->session->{ _zaak_create }) {
        my $verified = $c->session->{ _zaak_create }{ extern }{ verified };

        $c->stash->{ is_preset_client } = $verified eq 'preset_client';
    }

    $c->stash->{zaaktype}   = $c->stash->{zaak}->zaaktype_node_id;
    $c->stash->{nowrapper}  = 1;
    $c->stash->{betaling}   = 1;

    $c->forward('/zaak/finish');
}


=head2 assert_amount

Make sure the amount ogone returns matches up with whatever is stored in the zaak.
During case creation it is possible to modify the amount using rules and
the contactchannel. The final amount isstored in the case.

=cut

sub assert_amount : Private {
    my ($self, $c) = @_;

    # check the amount that ogone return to the stored amount
    # in the zaak.payment_amount field
    my $ogone_amount  = $c->stash->{ogone}->amount;

    my $stored_amount
        = $self->amount_in_cents($c->stash->{zaak}->payment_amount);

    throw('plugin/ogone/corrupt_state',
        'Ogone payment amount and locally stored amount do not match, aborting. ' .
        "Ogone: $ogone_amount, stored: $stored_amount",
    ) unless $ogone_amount eq $stored_amount;

    $c->log->debug(sprintf(
        'Ogone transaction validated and accepted, payment received: %d¢',
        $stored_amount
    ));
}

sub _delete_delayed_case {
    my ($self, $c, $state) = @_;

    my $case_id = $c->stash->{postponed}->data->{case_id};
    $c->model('DB::Logging')->trigger(
        'case/delete',
        {
            component => 'zaak',
            data      => {
                case_id       => $case_id,
                acceptee_name => 'Ogone',
                reason        => 'declined',
            },
        }
    );
    $c->stash->{postponed}->delete();
}

sub decline : Chained('base') : PathPart('decline'): Args(0) {
    my ($self, $c) = @_;

    if ($c->stash->{postposed}) {
        $self->_delete_delayed_case($c, 'declined');
        $c->stash->{ogone_decline} = 1;
        $c->delete_session('User declined ogone transaction');
        return;
    }

    unless($c->stash->{zaak}->payment_status eq CASE_PAYMENT_STATUS_PENDING) {
        $c->log->warn('Received payment-declined for case %d, but transaction status was not pending, giving up.');
        $c->detach;
    }

    $c->stash->{ogone_decline} = 1;
    $c->stash->{zaak}->set_payment_status(CASE_PAYMENT_STATUS_FAILED);
    $c->stash->{zaak}->set_deleted(process => 'Ogone', no_checks => 1);

    $c->delete_session('User declined ogone transaction');
}

sub exception : Chained('base') : PathPart('exception'): Args(0) {
    my ($self, $c) = @_;

    if ($c->stash->{postponed}) {
        # On exception handling, the case get's created but payment is pending
        $c->stash->{postponed}->update({ status => 'pending' })
            ->discard_changes;
        $c->model('Queue')->queue_item($c->stash->{postponed});
    }
}

=head2 cancel

The Ogone payment has been cancelled. The associated case will be deleted.

=cut

sub cancel : Chained('base') : PathPart('cancel'): Args(0) {
    my ($self, $c) = @_;

    if ($c->stash->{postposed}) {
        $c->stash->{ogone_error}  = 'Uw betaling is geannuleerd.';

        $self->_delete_delayed_case($c, 'cancel');

        $c->stash->{ogone_cancel} = 1;
        $c->delete_session('User cancelled ogone transaction');
        return;
    }

    $c->stash->{ogone_error}  = 'Uw betaling is geannuleerd.';
    $c->stash->{ogone_cancel} = 1;
    $c->stash->{zaak}->set_deleted(process => 'Ogone', no_checks => 1);
    $c->delete_session('User canceled ogone transaction');
}

sub cancel_by_zaaksysteem : Private {
    my ($self, $c) = @_;

    $self->_delete_delayed_case($c, 'expired');
    return;
}

sub update_payment_status_and_queue {
    my ($self, $c, $status) = @_;

    my $jsonb = _get_sql_for_payment_status($status);
    $c->stash->{postponed}->update(
        {
            status => 'pending',
            data   => \$jsonb,
        }
    )->discard_changes;

    # It is a wrap
    $c->model('Queue')->queue_item($c->stash->{postponed});
}

sub offline_by_zaaksysteem : Private {
    my ($self, $c) = @_;

    $self->update_payment_status_and_queue($c, CASE_PAYMENT_STATUS_OFFLINE);
    # Because zaaktype and zaaktype NODE are the same.. amiright?
    $c->stash->{zaaktype}
        = $self->_get_casetype_for_delayed_case($c)->zaaktype_node_id;
    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CASE_PAYMENT_STATUS_FAILED

TODO: Fix the POD

=cut

=head2 CASE_PAYMENT_STATUS_PENDING

TODO: Fix the POD

=cut

=head2 CASE_PAYMENT_STATUS_SUCCESS

TODO: Fix the POD

=cut

=head2 accept

TODO: Fix the POD

=cut

=head2 amount_in_cents

TODO: Fix the POD

=cut

=head2 betaling

TODO: Fix the POD

=cut

=head2 cancel

TODO: Fix the POD

=cut

=head2 decline

TODO: Fix the POD

=cut

=head2 exception

TODO: Fix the POD

=head2 base

TODO: Fix the POD

=cut
