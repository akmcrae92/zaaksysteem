package Zaaksysteem::DB::Component::Logging::Object::Queue;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head1 NAME

Zaaksysteem::DB::Component::Loogging::Object::Queue - Generic methods
available on queue item log events

=head1 METHODS

=head2 get_item

Retrieve a queue item by id

=cut

sub get_item {
    shift->result_source->schema->resultset('Queue')->find(shift);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
