package Zaaksysteem::Backend::Rules::Rule::Action::ShowHideTextblock;
use Moose;
use namespace::autoclean;

with 'Zaaksysteem::Backend::Rules::Rule::Action';

use Zaaksysteem::Types qw(NonEmptyStr);

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Action::ShowHideTextblock - Show/hide a text block

=head1 DESCRIPTION

This rule action shows or hides a text block. This is all done in frontend.

=head1 ATTRIBUTES

=head2 attribute_name

The name of the text field to show/hide -- this is used as a key.

Should be a non-empty string (the configured "title" of the text block to show/hide)

=cut

has 'attribute_name'    => (
    is          => 'rw',
    isa         => NonEmptyStr,
    required    => 1,
);


=head1 METHODS

=head2 integrity_verified

Whether the rule is still OK to run. Always returns 1.

=cut

sub integrity_verified { return 1; }

sub _populate_validation_results {
    my $self            = shift;
    my $result_object   = shift;
    my $params          = shift;

    if ($self->type eq 'hide_text_block') {
        ### Make sure the show_group function does not show this element again
        $result_object->_hidden_text_blocks_metadata->{'hide'}->{$self->attribute_name} = 1;

        $result_object->_remove_active_text_blocks($self->attribute_name);

        $result_object->_add_hidden_text_blocks($self->attribute_name);
    }
    elsif ($self->type eq 'show_text_block') {
        delete $result_object->_hidden_text_blocks_metadata->{'hide'}->{$self->attribute_name};

        ### Do not show an attribute when it is already hidden by a group hide
        unless ($result_object->_hidden_text_blocks_metadata->{'hide_group'}->{$self->attribute_name}) {
            $result_object->_add_active_text_blocks($self->attribute_name);
        }
    }
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
