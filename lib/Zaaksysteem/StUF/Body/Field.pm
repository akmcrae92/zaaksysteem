package Zaaksysteem::StUF::Body::Field;

use Moose;

has 'name'              => (
    is      => 'rw',
);

has '_value'            => (
    is      => 'rw',
);

has 'value'             => (
    is      => 'rw',
);

has 'is_kerngegeven'    => (
    is      => 'rw',
);

has 'exact'             => (
    is      => 'rw',
);

has 'exists'            => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        return 1;
    }
);

has 'no_value'          => (
    is => 'rw',
);

sub BUILD {
    my $self            = shift;

    if (!ref($self->_value)) {
        $self->value($self->_value);
    } else {
        $self->_parse_value();
    }
}

sub _parse_value {
    my $self            = shift;

    return unless $self->_value;

    return unless (
        UNIVERSAL::isa($self->_value, 'HASH')
    );

    $self->exact( $self->_value->{exact} );

    if (exists($self->_value->{noValue})) {
        if ($self->_value->{noValue} eq 'geenWaarde') {
            #warn('Setting: ' . $self->name . ' to undef');
            $self->value(undef);
            return;
        } else {
            $self->exists(0);
            return;
        }
    }

    return unless exists($self->_value->{_});

    if (ref($self->_value->{_}) eq 'Math::BigFloat') {
        $self->value( "" . $self->_value->{_}->bstr . "");
    } else {
        $self->value( "" . $self->_value->{_} . "");
    }
    #$self->kerngegeven( $self->_value->{kerngegeven} );

}

# For XML::Compile
sub TO_STUF {
    my $self            = shift;

    my $rv = {
        '_'     => $self->value,
    };

    $rv->{exact}        = $self->exact if $self->exact;

    return $rv;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

=head2 TO_STUF

TODO: Fix the POD

=cut

