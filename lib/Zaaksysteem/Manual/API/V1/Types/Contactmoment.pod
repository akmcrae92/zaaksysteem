=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Contactmoment - Type definition for
contactmoment objects

=head1 DESCRIPTION

This page documents the serialization of C<contactmoment> objects.

=head1 JSON

=begin javascript

{
    "type": "contactmoment",
    "reference": "2a6a0ac0-d757-42e9-8dd9-0e4cf3c5bf0a",
    "instance": {
        "subject_id": "betrokkene-medewerker-1",
        "case_id": 1337,
        "type": "note",
        "medium": "balie",
        "date_created": "2016-09-13T17:06:28Z",
        "created_by": "betrokkene-medewerker-2",
        "message": "my note",
        "email_body": null,
        "email_subject": null,
        "email_recipient": null,
        "email_cc": null,
        "email_bcc": null
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 subject_id E<raquo> L<C<betrokkene_identifier>|Zaaksysteem::Manual::API::V1::ValueTypes/betrokkene_identifier>

The C<betrokkene_identifier> of the subject for who the object was created.

=head2 case_id E<raquo> L<C<case:number>|Zaaksysteem::Manual::API::V1::Types::Case/number>

The C<case:number> of the case related to the contactmoment.

=head2 type E<raquo> L<C<enum>|Zaaksysteem::Manual::API::V1::Types::ValueTypes/enum>

Type of contactmoment.

=head2 medium E<raquo> LC<enum(balie, telefoon, post, email, webformulier, behandelaar, sociale media)>

The medium used to register the contactmoment.

=head2 date_created 

E<raquo> C<iso8601>

This attribute contains the creation timestamp.

=head2 created_by

E<raquo> C<betrokkene_identifier>

C<betrokkene_identifier> of the subject who created the contactmoment.

=head2 message 

E<raquo> C<text>

Body of the contactmoment.

=head2 email_body

=head2 email_subject

=head2 email_recipient

=head2 email_cc

=head2 email_bcc

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
