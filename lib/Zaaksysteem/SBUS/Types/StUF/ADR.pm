package Zaaksysteem::SBUS::Types::StUF::ADR;

use Data::Dumper;
use Moose;

use Zaaksysteem::SBUS::Constants;

extends 'Zaaksysteem::SBUS::Types::StUF::GenericAdapter';

sub _stuf_to_params {
    my ($self, $prs_xml, $stuf_options) = @_;

    my $params = $self->handle_adr_relations(undef, $prs_xml);

    return $params;
}

sub _stuf_relaties {}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
