package Zaaksysteem::API::v1::Serializer::Reader::EmailTemplate;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::EmailTemplate - Read email templates

=head1 DESCRIPTION

=head1 METHODS

=head2 class

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Model::DB::BibliotheekNotificaties' }

=head2 read

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub read {
    my ($class, $serializer, $item) = @_;

    return {
        type => 'email_template',
        reference => $item->uuid,
        instance => {
            label          => $item->label,
            subject        => $item->subject,
            message        => $item->message,
            sender         => $item->sender,
            sender_address => $item->sender_address,
            # Not yet: category
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
