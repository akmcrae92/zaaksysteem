package Zaaksysteem::Object::Types::Sysin::RemoteCallResponse;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use Zaaksysteem::Types qw(NonEmptyStr);

=head1 NAME

Zaaksysteem::Object::Types::Sysin::RemoteCallResponse - Type for "RPC call" responses

=head1 DESCRIPTION

RPC calls can have wildly varied responses. 

=head1 ATTRIBUTES

=head2 call

=cut

has call => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Name of the call',
    required => 1,
);

=head2 data

=cut

has data => (
    is       => 'rw',
    traits   => [qw(OA)],
    label    => 'Response data',
    required => 1,
);

=head1 METHODS

=head2 type

Overrides L<Zaaksysteem::Object/type>, always returns the string C<sysin/remote_call_response>.

=cut

override type => sub { 'sysin/remote_call_response' };

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
