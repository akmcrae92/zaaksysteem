# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy

msgid "intern"
msgstr "Intern"

msgid "extern"
msgstr "Extern"

msgid "natuurlijk_persoon"
msgstr "Natuurlijk Persoon (Binnengemeentelijk)"

msgid "natuurlijk_persoon_na"
msgstr "Natuurlijk Persoon (Buitengemeentelijk)"

msgid "niet_natuurlijk_persoon"
msgstr "Niet natuurlijk persoon (Organisatie)"

msgid "medewerker"
msgstr "Behandelaar"

msgid "aanvrager_adres"
msgstr "Adres aanvrager"

msgid "anders"
msgstr "Andere locatie"

msgid "aangeven"
msgstr "Aangeven"

msgid "aanmelden"
msgstr "Aanmelden"

msgid "aanvragen"
msgstr "Aanvragen"

msgid "afkopen"
msgstr "Afkopen"

msgid "afmelden"
msgstr "Afmelden"

msgid "indienen"
msgstr "Indienen"

msgid "inschrijven"
msgstr "Inschrijven"

msgid "ontvangen"
msgstr "Ontvangen"

msgid "aanschrijven"
msgstr "Aanschrijven"

msgid "vaststellen"
msgstr "Vaststellen"

msgid "uitvoeren"
msgstr "Uitvoeren"

msgid "opstellen"
msgstr "Opstellen"

msgid "melden"
msgstr "Melden"

msgid "reserveren"
msgstr "Reserveren"

msgid "stellen"
msgstr "Stellen"

msgid "voordragen"
msgstr "Voordragen"

msgid "vragen"
msgstr "Vragen"

msgid "starten"
msgstr "Starten"

msgid "vaststellen"
msgstr "Vaststellen"

msgid "opzeggen"
msgstr "Opzeggen"

msgid "aangaan"
msgstr "Aangaan"

msgid "registreren"
msgstr "Registreren"

msgid "versturen"
msgstr "Versturen"

msgid "collegebesluit"
msgstr "Collegebesluit"

msgid "raadsbesluit"
msgstr "Raadsbesluit"

msgid "mandaatbesluit"
msgstr "Mandaatbesluit"

msgid "openbaar"
msgstr "Openbaar"

msgid "gesloten"
msgstr "Niet openbaar"

msgid "kalenderdagen"
msgstr "Kalenderdagen"

msgid "weken"
msgstr "Weken"

msgid "werkdagen"
msgstr "Werkdagen"

msgid "einddatum"
msgstr "Vaste einddatum"

msgid "authenticatie"
msgstr "Altijd authenticatie"

msgid "optie"
msgstr "Authenticatie wanneer beschikbaar"

msgid "kenmerken"
msgstr "Kenmerken"

msgid "kenmerken1"
msgstr "Kenmerk"

msgid "sjablonen"
msgstr "Sjablonen"

msgid "sjablonen1"
msgstr "Sjabloon"

msgid "besluit"
msgstr "Besluit"

msgid "resultaat"
msgstr "Resultaat"

msgid "status_new"
msgstr "Nieuw"

msgid "status_open"
msgstr "Open"

msgid "status_resolved"
msgstr "Afgehandeld"

msgid "status_deleted"
msgstr "Verwijderd"

msgid "status_stalled"
msgstr "Opgeschort"

msgid "status_overdragen"
msgstr "Overdragen"

msgid "minuten"
msgstr "Minuten"

msgid "maanden"
msgstr "Maanden"

### Validatie

msgid "validity %1 invalid"
msgstr "Veld is ongeldig"

msgid "validity %1 valid"
msgstr "Veld is geldig"

msgid "validity %1 missing"
msgstr "Verplicht veld is niet ingevuld"

### Validatie - Controlpanel
msgid "Given domain is already in use, please choose another one"
msgstr "Opgegeven domeinnaam is al in gebruik, voer aub een andere in"

msgid "Invalid hostname, make sure it contains only characters and/or digits, like example"
msgstr "Ongeldige domeinnaam, zorg dat de ingevulde naam enkel letters of cijfers bevat, zoals "mintlab21"'

msgid "Domain is invalid, make sure you enter a correct domain, like test.example.com"
msgstr "Ongeldige domeinnaam, voer een geldig domein in, zoals test.zaaksysteem.nl"

msgid "Invalid hostname"
msgstr "Ongeldige domeinnaam"

msgid "Invalid hostname, make sure the hostname you enter does not contain a dot"
msgstr "Ongeldige hostnaam, zorg dat de hostnaam geen . bevat"

msgid "Hostname given is reserved"
msgstr "Opgegeven domeinnaam is gereserveerd"

### Validatie - Subject
msgid "Dossiernumber already in use"
msgstr "Opgegeven combinatie KVK-nummer en vestigingsnummer zijn al in gebruik"
